<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function get_user($email, $pwd)
    {
        $this->db->where('email', $email);
        $this->db->where('password', $pwd);
        $query = $this->db->get('tb_admin');
        return $query->result();
    }
    
    function getClubInfoByMail($admin_email) {
        
        $this->db->select('A.*, B.name admin_name_by, B.email admin_email_by');
        $this->db->from('tb_club A'); 
        $this->db->join('tb_admin B', 'A.admin_email=B.email', 'left');
        $this->db->where('A.admin_email', $admin_email);
        $query = $this->db->get(); 
        
        return $query->result();        
    }
    
    function getClubInfo() {
        
        $this->db->select('A.*, B.name admin_name_by, B.email admin_email_by');
        $this->db->from('tb_club A'); 
        $this->db->join('tb_admin B', 'A.admin_email=B.email', 'left');
        $query = $this->db->get(); 
        
        return $query->result();        
    }
    
    function getSecurityInfo($admin_email) {
        
        $club_id = $this->db->where('admin_email', $admin_email)->get('tb_club')->row()->id;
        
        return $this->db->where('club_id', $club_id)->get('tb_user')->result();
    }
    
    function getPatronsInfo() {
        
//        $this->db->select('A.*,  B.total_insident');
//        $this->db->from('tb_patron A, tb_incident B');
//        $this->db->where('A.id = B.patron_id');
//        return $this->db->get('tb_patron')->result();

        ///whethere the patron is created by this club admin
        
        
        $where = array();//security & admission's ids
        $this->db->where('club_admin_id', $this->session->userdata('uid'));
        $query = $this->db->get('tb_user');
        if ($query->num_rows() > 0) {
            $i = 0;        
            foreach($this->db->get('tb_user')->result() as $row) {
                $where[$i] = $row->id;
                $i++;
            }
        }
        
//        return $this->db
//         ->select('tb_patron.*, COUNT(tb_incident.patron_id) as total_incident')
//         ->from('tb_patron')
//         ->join('tb_incident', 'tb_patron.id = tb_incident.patron_id','left')
//         ->group_by('tb_patron.id')
//         ->where_in('user_id', $where)
//         ->get()
//         ->result();
         
         return $this->db
         ->select('tb_patron.*, COUNT(tb_incident.patron_id) as total_incident')
         ->from('tb_patron')
         ->join('tb_incident', 'tb_patron.id = tb_incident.patron_id','left')
         ->group_by('tb_patron.id')          
         ->get()
         ->result();
         
    }
    
    function getIncidentInfo() {
        
        $this->db->select('A.*, B.daq, B.scan_id');
        $this->db->from('tb_incident A'); 
        $this->db->join('tb_patron B', 'A.patron_id=B.id', 'left');
        //$this->db->order_by('A.reg_date','DESC');         
        $query = $this->db->get(); 
        
        return $query->result();        
    }
    
    function allowDispute($id) {
        
        $this->db->where('id', $id);
        $this->db->set('dispute_request_state', 2);
        $this->db->set('action_date', 'NOW()', false);
        $this->db->update('tb_incident');
    }
    
    function declineDispute($id) {
        
        $this->db->where('id', $id);
        $this->db->set('dispute_request_state', -1);
        $this->db->set('action_date', 'NOW()', false);
        $this->db->update('tb_incident');
    }
    
    /// super admin
    
    function getSuperAdminInfo() {
        
        $this->db->where('permission', 1);
        return $this->db->get('tb_admin')->result();   
    }
    
    function getSuperNameExist($name) {
        
        $this->db->where('name', $name);
        return $this->db->get('tb_admin')->num_rows();
    }
    
    function getSuperEmailExist($email) {
        
        $this->db->where('email', $email);
        return $this->db->get('tb_admin')->num_rows();
    }
    
    function addNewSuper($name, $email, $password) {
        
        $this->db->set('name', $name);
        $this->db->set('email', $email);
        $this->db->set('password', $password);
        $this->db->set('permission', 1);
        $this->db->set('create_at', 'NOW()', false);
        $this->db->insert('tb_admin');
        $this->db->insert_id();
    }   
    
    function getSuperAdminOne($id) {
        
        $this->db->where('id', $id);
        return $this->db->get('tb_admin')->row();
    }
    
    function updateSuperAdmin($id, $name, $email, $password) {
        
        $this->db->where('id', $id);
        $this->db->set('name', $name);
        $this->db->set('email', $email);
        $this->db->set('password', $password);
        $this->db->set('permission', 1);
        $this->db->update('tb_admin');
        
        
    }
    
    function deleteSuperAdmin($id) {
        
        $this->db->where('id', $id);
        $this->db->delete('tb_admin'); 
    }
    
    //////////////////// Club admin
    function getClubAdminInfo() {
        
        $this->db->where('permission', 2);
        return $this->db->get('tb_admin')->result();   
    }
    
    function addNewClub($name, $email, $password) {
        
        $this->db->set('name', $name);
        $this->db->set('email', $email);
        $this->db->set('password', $password);
        $this->db->set('permission', 2);
        $this->db->set('create_at', 'NOW()', false);
        $this->db->insert('tb_admin');
        $this->db->insert_id();
    }    
    
    function updateClubAdmin($id, $name, $email, $password) {
        
        $this->db->where('id', $id);
        $this->db->set('name', $name);
        $this->db->set('email', $email);
        $this->db->set('password', $password);
        $this->db->set('permission', 2);
        $this->db->update('tb_admin'); 
    } 
    
    ////////// add patron //////////
    
    function getPatronExist($iin, $daq, $dba, $dbb) {
        
        $this->db->where('scan_id', $iin);
        $this->db->where('daq', $daq);
        $this->db->where('dba', $dba);
        $this->db->where('dbb', $dbb);
        return $this->db->get('tb_patron')->num_rows();
        
    }
    
    function addNewPatron($iin, $daq, $dba, $dbb) {
        
        $this->db->set('scan_id', $iin);
        $this->db->set('daq', $daq);
        $this->db->set('dba', $dba);
        $this->db->set('dbb', $dbb);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_patron');
        $this->db->insert_id();
    }
    
    function getPatronOne($id) {
        
        $this->db->where('id', $id);
        return $this->db->get('tb_patron')->row();
    }
    
    function updatePatron($id, $iin, $daq, $dba, $dbb) {
        
        $this->db->where('id', $id);
        $this->db->set('scan_id', $iin);
        $this->db->set('daq', $daq);
        $this->db->set('dba', $dba);
        $this->db->set('dbb', $dbb);
        $this->db->update('tb_patron');        
    }  
    
    function deletePatron($id) {
        
        $this->db->where('id', $id);
        $this->db->delete('tb_patron');
    }
    
    function resetSuperPassword($id, $new_password) {
        
        $this->db->where('id', $id);
        $this->db->set('password', $new_password);
        $this->db->update('tb_admin');
    }
    
    function getSecurityExist($email) {
        
        $this->db->where('email', $email);
        return $this->db->get('tb_user')->num_rows();        
    }
    
    function getClubName($club_id) {
        
        return $this->db->where('id', $club_id)->get('tb_club')->row()->name;
    }
    
    function addNewSecurity($username, $email, $password_hash, $permission, $club_id, $club_name) {
        
        $this->db->set('username', $username);
        $this->db->set('email', $email);
        $this->db->set('password', $password_hash);
        $this->db->set('permission', $permission);
        $this->db->set('club_id', $club_id);
        $this->db->set('club_name', $club_name);
        $this->db->set('club_admin_id', $this->session->userdata('uid'));
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_user');
        $this->db->insert_id();
    }
    
    function deleteSecurity($id) {
        
        $this->db->where('id', $id);
        $this->db->delete('tb_user');
        
        /*
        $this->db->where('user_id', $id);
        $this->db->delete('tb_patron');
        */
    }
    
    function deleteClub($id) {
        
        $this->db->where('id', $id);
        $this->db->delete('tb_club');
    }
    
    function getState() {
        
        return $this->db->get('tb_states')->result();
    }
    
    function getClubOneNameExist($name) {
        
        $this->db->where('name', $name);
        return $this->db->get('tb_club')->num_rows();        
    }
    
    function addNewClubOne($name, $address1, $address2, $city, $state, $admin_id, $access_level, $admin_name, $admin_email, $admin_pwd) {
        
        $this->db->set('name', $name);
        $this->db->set('address1', $address1);
        $this->db->set('address2', $address2);
        $this->db->set('city', $city);
        $this->db->set('state', $state);
        $this->db->set('membership_date', 'NOW()', false);
        $this->db->set('status', 1);
        $this->db->set('access_level', $access_level);
        $this->db->set('admin_name', $admin_name);
        $this->db->set('admin_email', $admin_email);
        $this->db->set('admin_id_by', $admin_id);
        $this->db->insert('tb_club');
        $this->db->insert_id();
        
        // add admin table too
        $this->db->set('name', $admin_name);
        $this->db->set('email', $admin_email);
        $this->db->set('password', $admin_pwd);
        $this->db->set('permission', 2);
        $this->db->set('create_at', 'NOW()', false);
        $this->db->insert('tb_admin');
        $this->db->insert_id();
    }
}
  
?>
