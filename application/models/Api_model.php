<?php

class Api_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    function usernameExist($name) {

        $this->db->where('username', $name);         
        return $this->db->get('tb_user')->num_rows();
    }

    function emailExist($email) {

        $this->db->where('email', $email);
        return $this->db->get('tb_user')->num_rows();
    }
    
    function register($name, $email, $password) {

        $this->db->set('username', $name);
        $this->db->set('email', $email);        
        $this->db->set('password', $password);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_user');
        return $this->db->insert_id();
    }
    
    function updatePhoto($user_id, $file_url) {
        
        $this->db->where('id', $user_id);
        $this->db->set('photo_url', $file_url);
        $this->db->update('tb_user');
    }
    
    function patronExist($scan_id, $daq, $club_id) {                 
        
        $this->db->where('scan_id', $scan_id); 
        $this->db->where('club_id', $club_id); 
        $this->db->where('daq', $daq);        
        return $this->db->get('tb_patron')->num_rows();
    }
    
    function patronExistWith($iin, $daq) {                 
        
        $this->db->where('scan_id', $iin);         
        $this->db->where('daq', $daq);
        $query =  $this->db->get('tb_patron');        
        
        if ($query->num_rows() > 0) {
            
            return $query->row()->id;
        } else {
            return 0;
        }
    } 
    
    function registerPatron($user_id, $club_id, $scan_id, $daq, $dba, $dbb) {
        
        $this->db->set('user_id', $user_id);
        $this->db->set('club_id', $club_id);
        $this->db->set('scan_id', $scan_id);
        $this->db->set('daq', $daq);        
        $this->db->set('dba', $dba);
        $this->db->set('dbb', $dbb);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->set('last_present_date', 'NOW()', false);
        $this->db->insert('tb_patron');
        return $this->db->insert_id();        
    }
    
    function getPatronId($club_id, $scan_id, $daq) {
        
        $this->db->where('scan_id', $scan_id);
        $this->db->where('club_id', $club_id);
        $this->db->where('daq', $daq);
        return $this->db->get('tb_patron')->row()->id;
    }
    
    function getPatronPhoto($scan_id, $daq) {
        
        $this->db->where('scan_id', $scan_id);
        $this->db->where('daq', $daq);
        return $this->db->get('tb_patron')->row()->photo_url;
    }
    
    function getPatronInfo($p_id) {
        
        $result = array();
        $incident_list = array();
        
        $this->db->where('id', $p_id);
        $patron = $this->db->get('tb_patron')->row();
        
        $incident_query = $this->db->where('patron_id', $p_id)->get('tb_incident');
        
        if ($incident_query->num_rows() > 0) {
            
            foreach($incident_query->result() as $incident) {
                
                $arr = array('incident_id' => $incident->id,
                             'incident_description' => $incident->description,
                             'incident_picture' => $incident->incident_pic,
                             'incident_type' => $incident->incident_type,
                             'incident_dispute_status' => $incident->dispute_request_state,
                             'incident_dispute_comment' => $incident->dispute_comment,
                             'incident_date' => $incident->reg_date);
                array_push($incident_list, $arr);
            }
        }
        
        $result = array('patron_id' => $patron->id,
                        'total_visit_count' => $patron->total_visit,
                        'photo_url' => $patron->photo_url,        
                        'last_present_date' => $patron->last_present_date,        
                        'registered_date' => $patron->reg_date,
                        'incident_list' => $incident_list,
                        'total_incident_count' => count($incident_list)
                        );
                        
        return $result;         
    }
    
    function acceptPatron($p_id) {
        
        $cur_visit_count = $this->db->where('id', $p_id)->get('tb_patron')->row()->total_visit;
        $cur_visit_count++;
        
        $this->db->where('id', $p_id);
        $this->db->set('total_visit', $cur_visit_count);
        $this->db->set('last_present_date', 'NOW()', false);
        $this->db->update('tb_patron');
    }   
    
    
    function updatePatronPhoto($p_id, $file_url) {
        
        $this->db->where('id', $p_id);
        $this->db->set('photo_url', $file_url);
        $this->db->update('tb_patron');
    }
    
    function createIncident($p_id, $p_type, $p_description, $file_url, $club_id, $incident_id) {
        
        $this->db->set('patron_id', $p_id);
        $this->db->set('club_id', $club_id);
        $this->db->set('incident_type', $p_type);
        $this->db->set('incident_id', $incident_id);
        $this->db->set('description', $p_description);
        $this->db->set('incident_pic', $file_url);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_incident');
        return $this->db->insert_id();
    }
    
    function getPatrons($user_id, $club_id, $page_index) {
        
        $result = array();
        $start =  ($page_index - 1) * 30;        
        $this->db->limit(30, $start);
        $this->db->where('club_id', $club_id);
        $this->db->where("last_present_date > DATE_SUB(NOW(),INTERVAL 18 HOUR)", NULL, FALSE);
        $this->db->order_by('last_present_date', 'DESC');
        $query = $this->db->get('tb_patron');
        
        if ($query->num_rows() > 0) {
            
            foreach($query->result() as $patron) {
                
                $arr = array('patron_id' => $patron->id,
                             'scan_id' => $patron->scan_id,
                             'daq' => $patron->daq,
                             'total_visit_count' => $patron->total_visit,
                             'photo_url' => $patron->photo_url,        
                             'last_present_date' => $patron->last_present_date,        
                             'registered_date' => $patron->reg_date,
                             );
                array_push($result, $arr);
            }
        }
        
        return $result;
    }  
    
    function submitDispute($p_id, $p_comment) {
        
        $this->db->where('id', $p_id);
        $this->db->set('dispute_comment', $p_comment);
        $this->db->set('dispute_request_state', 1);
        $this->db->update('tb_incident');
    }
    
    function getSubmitState($p_id) {
        
        $this->db->where('id', $p_id);
        return $this->db->get('tb_incident')->row()->dispute_request_state;
        
    }
    
}
?>
