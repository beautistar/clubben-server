<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-header">Club admin manage   >  Add Club Admin</h4>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    
        <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Information   
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                
                <div class="row">
                    <div class="col-md-7" >
                    
                        <form    role="form" name = "aForm" method="post" action="<?php echo base_url();?>index.php/admin/addClubConfirm" >
                                          
                            <div class="row" style="margin-top: 30px;">
                                <div class="col-lg-2">
                                    User name :     
                                </div>

                                <div class="col-lg-10">
                                    <input type="text" id = "name" class="col-lg-10" name = name placeholder="User Name" required>
                                </div>

                            </div>

                            <div class="row" style="margin-top: 50px;">
                                <div class="col-lg-2">
                                    Email Address :     
                                </div>

                                <div class="col-lg-10">
                                    <input type="text" id = "email" class="col-lg-10" name = email placeholder="Email address" required>
                                </div>

                            </div>
                            
                            <div class="row" style="margin-top: 50px;">
                                <div class="col-lg-2">
                                    Password :      
                                </div>

                                <div class="col-lg-10">
                                    <input class="col-lg-10" id="password" type="password" name = password placeholder="Password"  required>
                                </div>
                            </div>
                                                                                                                  
                           <div class=row>
                                <div class="col-lg-11 " style="margin-top: 50px; margin-bottom: 30px;">
                                        <input class="col-lg-12 btn-primary" type=submit value = "Add" >                                           
                                </div>
                            </div>                      
                              
                        </form>                  
                    </div>  
                </div>    
                    
                    
                </div>
                <!-- /.panel-body -->
                
                <div class="panel-footer " style="text-align:center;">                    
                    <input type="hidden">
                </div>                
                
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>     
    
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->

    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });

    function onlyNumber(event){
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        var number_length = document.getElementById("phone_number").value.length;
        if ( (keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) ||  keyID == 8 || keyID == 46 ||  number_length < 11 ) 
            return;
        else
            return false;
    }
    function removeChar(event) {
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        if ( keyID == 8 || keyID == 46 ) 
            return;
        else
            event.target.value = event.target.value.replace(/[^0-9]/g, "");
    }
    
    function valueSelect(myval) {
        var name = document.getElementById('name').value;
        var phone_number = document.getElementById('phone_number').value;
        var device_number = document.getElementById('device_number').value;
        
        if (name.length == 0) name="A";
        if (phone_number.length == 0) phone_number="A";
        if (device_number.length == 0) device_number="A" ;
        location.href = "<?php echo base_url();?>"+"index.php/admin/addTeacherWithRegion/"+myval+'/'+name+'/'+phone_number+'/'+device_number;
//                newWin = window.open("<?php echo base_url();?>"+"admin/updateBranch/"+myval, '_blank', "toolbar=no,location=1,scrollbars=1"
//            + ",width=800,height=650,top=100,left=100");
    }
    </script>    
    
    
    
    
<?php
    if($this->session->flashdata('msg')){
    ?>
    <script>
        alert('<?=$this->session->flashdata('msg')?>');
    </script>
    <?php
    }
        
?>

