
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-header">Incident</h4>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Incident List   
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th style="text-align: center;" >No</th>
                                    <!--<th style="text-align: center;" >Club ID</th>-->
                                    <th style="text-align: center;" >Incident ID</th>
                                    <th style="text-align: center;" >Patron ID</th>
                                    <th style="text-align: center;" >Incident Pic</th>  
                                    <th style="text-align: center;" >Description</th>  
                                    <!--<th style="text-align: center;" >Created at:</th>--> 
                                    <th style="text-align: center;" >Dispute Status</th> 
                                    <th style="text-align: center;" >Dispute Comment</th> 
                                    <!--<th style="text-align: center;" >Dispute Allow</th>--> 
                                                                                    
                                    <th style="text-align: center;" >Action</th>  
                                    <th style="text-align: center;" >Action Date</th>
                                    <!--<th style="text-align: center;" >Note</th>-->                                              
                                </tr>
                            </thead>
                            <tbody>
                                
                            
                            <?php
                            $i = 1;
                                foreach ($data as $item)  {?> 
                            
                                    <tr class="odd gradeX">
                                    <td style="text-align: center; text-align: center; vertical-align: middle;"><?=$i?></td>
                                    <!--<td style="text-align: center; text-align: center; vertical-align: middle;"><?=1?></td>-->
                                    
                                    <td style="text-align: center; text-align: center; vertical-align: middle;"><?=$item->incident_id?></td>                                                                                   
                                    <td style="text-align: center; vertical-align: middle;"><?=$item->scan_id.$item->daq?></td>
                                    <?php
                                    if($item->incident_pic){
                                    ?>
                                    <td style="vertical-align: middle; text-align: center; width: 50px; height: 50px;">
                                    <img class="img-rounded img-thumbnail" onclick="showImage(this)" src="<?=$item->incident_pic?>" width=200,height=100")" /></td>
                                    <?php
                                       } else {
                                    ?>                                      
                                    <td style="vertical-align: middle; text-align: center; vertical-align: middle;"><img class="img-rounded img-thumbnail" src="<?php echo base_url();?>/frontend/images/avatar_default.jpg" alt=""></td>    
                                    <?php
                                    }
                                    ?>
                                    
                                    <td class="col-lg-4" style="text-align: center; vertical-align: middle;"><?=$item->description?></td>
                                    <!--<td style="text-align: center; vertical-align: middle;"><?=explode(" ", $item->reg_date)[0]?></td>-->
                                    <td style="text-align: center; vertical-align: middle; color: red;">
                                    <?php 
                                     switch ($item->dispute_request_state) {
                                         case 1: echo "Requested";break;
                                         case 2 : echo "Confirmed";break;
                                         case -1 : echo "Reversed"; break;                                         
                                     }
                                     ?>
                                     </td>
                                    <td style="text-align: center; vertical-align: middle;"><?=$item->dispute_comment?></td>                                    
                                     
                                    <td style="text-align: center; vertical-align: middle;">
                                        
                                        
                                        <div class="dropdown">
                                           <button class="btn-flat btn-primary dropdown-toggle" 
                                                  type="button" id="dropdownMenu1" data-toggle="dropdown">Action
                                                        <span class="caret"></span>
                                           </button>
                                           <?php if ($item->dispute_request_state) {?>
                                           <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                              <li role="presentation">
                                              <a role="menuitem" tabindex="-1"  
                                                    href="<?php echo base_url();?>index.php/admin/allowDispute/<?=$item->id?>">Confirm                                  
                                              </a>
                                              </li>                                                            
                                              <li role="presentation">
                                              <a role="menuitem" tabindex="-1" 
                                                    href="<?php echo base_url();?>index.php/admin/declineDispute/<?=$item->id?>">Reversed
                                              </a>
                                              </li>
                                           </ul>
                                          <?php } ?> 
                                    </div>                                                                     
                                </td>
                                <td style="text-align: center; vertical-align: middle;"><?php if($item->action_date != 0) echo $item->action_date; else echo ""?></td>                                    
                                <!--<td style="text-align: center; vertical-align: middle;"></td>-->                                     
                                </tr>                                      
                            <?php  $i++;}  ?>
                            
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    
                </div>
                <!-- /.panel-body -->                
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->             
</div>
<!-- /#page-wrapper -->

<script type="text/javascript">
    function showImage(img) {
        
        var src = img.src;
        window.open( src, "_blank", "toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=no, resizable=no, copyhistory=no, titlebar=no, top=100,left=200, width = 256, height = 256");

    }
</script>

