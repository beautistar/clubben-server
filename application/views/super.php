
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-header">Super Admins</h4>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Club List   
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th style="text-align: center;" >No</th>
                                    <th style="text-align: center;" >Super Admin ID</th>
                                    <th style="text-align: center;" >Name</th>
                                    <th style="text-align: center;" >Email Address</th>  
                                    <th style="text-align: center;" >Created at</th>  
                                    <th style="text-align: center;" >Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            
                            <?php
                            $i = 1;
                                foreach ($data as $item)  {?> 
                            
                                    <tr class="odd gradeX">
                                    <td style="text-align: center; text-align: center; vertical-align: middle;"><?=$i?></td>
                                    <td style="text-align: center; text-align: center; vertical-align: middle;"><?=$item->id?></td>                                            
                                    <td style="text-align: center; vertical-align: middle;"><?=$item->name?></td>
                                    <td style="text-align: center; vertical-align: middle;"><?=$item->email?></td>
                                    <td style="text-align: center; vertical-align: middle;"><?=explode(" ", $item->create_at)[0]?></td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <div class="dropdown">
                                           <button class="btn-flat btn-primary dropdown-toggle" 
                                                  type="button" id="dropdownMenu1" data-toggle="dropdown">Action
                                                        <span class="caret"></span>
                                           </button>
                                               <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                                  <li role="presentation">
                                                  <a role="menuitem" tabindex="-1" 
                                                        href="<?php echo base_url();?>index.php/admin/editSuperAdmin/<?=$item->id?>">Edit                                  
                                                  </a>
                                                  </li>                                                            
                                                  <li role="presentation">
                                                  <a role="menuitem" tabindex="-1"  onclick = "deleteClick(<?=$item->id?>)">Delete</a>
                                                  </li>

                                               </ul>
                                         </div>                                                                     
                                    </td>
                                </tr>                                      
                            <?php  $i++;}  ?>
                            
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    
                </div>
                <!-- /.panel-body -->
                
                <div class="panel-footer " style="text-align:center;">                    
                    <input type="button" class=" btn-flat btn-primary dropdown-toggle" onclick="addClick();" value="  Add  ">
                </div>
                
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->             
</div>
<!-- /#page-wrapper -->

<script type="text/javascript">
    function showImage(img) {
        
        var src = img.src;
        window.open( src, "_blank", "toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=no, resizable=no, copyhistory=no, titlebar=no, top=100,left=200, width = 256, height = 256");
    }

    function addClick() {
            
        location.href = "<?php echo base_url();?>index.php/admin/addSuperAdmin";         
    } 
    
    function deleteClick(id) {
        
        if (confirm("Are you sure want to delete?")) {    
        location.href = "<?php echo base_url();?>"+"index.php/admin/deleteSuperAdmin/" + id;         
        }
    }   
</script>   


