 <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
  <link rel="stylesheet" href="<?php echo base_url(); ?>frontend/css/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>frontend/css/animate.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>frontend/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>frontend/css/font.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>frontend/css/landing.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>frontend/css/app.css" type="text/css" />
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>frontend/images/favicon.ico">