<!DOCTYPE html>
<html lang="en" class="app">
<head>
  <meta charset="utf-8" />
  <title>ClubBEN</title>
  <?php $this->load->view('head'); ?>
</head>
<body class="">
  <section id="content" class="wrapper-md fadeInUp">    
    <div class="container aside-xxl">
<!--     <a class="navbar-brand block" href="http://eazypos.co.uk/"><img src="<?php echo base_url(); ?>frontend/images/logo2.png" alt="logo"></a>--> 
      <section class="panel panel-default bg-white m-t-lg">
        <header class="panel-heading text-center">
          <strong>Admin Sign in</strong>
        </header>

        <?php $attributes = array("name" => "loginform", "class"=>"panel-body wrapper-lg");
      echo form_open("admin/signinme", $attributes);?>
      <?php echo $this->session->flashdata('msg'); ?>
          <div class="form-group">
            <label class="control-label">Email</label>
            <input type="email" name="email" value="<?=set_value('email')?>" placeholder="test@example.com" class="form-control input-lg">
            <span class="text-danger"><?php echo form_error('email'); ?></span>
          </div>
          <div class="form-group">
            <label class="control-label">Password</label>
            <input type="password" name="password" id="inputPassword" placeholder="Password" class="form-control input-lg">
            <span class="text-danger"><?php echo form_error('password'); ?></span>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" name="remme"> Keep me logged in
            </label>
          </div>
          <!--<a href="<?php echo base_url();?>admin/forgot-password" class="pull-right m-t-xs"><small>Forgot password?</small></a>-->
          <button type="submit" class="btn btn-primary form-control" style="margin-top: 30px;">Sign in</button>
          <div class="line line-dashed"></div> 
          <!--          <p class="text-muted text-center"><small>Do not have an account?</small></p>-->
          <!--<a href="<?php echo base_url(); ?>signup" class="btn btn-default btn-block">Create an account</a>-->
        <?php echo form_close(); ?>
      </section>
    </div>
  </section>
   <?php $this->load->view('footerlink'); ?>