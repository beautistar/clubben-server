
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-header">Clubs</h4>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Club List   
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th style="text-align: center;" >No</th>
                                    <th style="text-align: center;" >Club ID</th>
                                    <th style="text-align: center;" >Name</th>
                                    <th style="text-align: center;" >Address 1</th>  
                                    <th style="text-align: center;" >Address 2</th>  
                                    <th style="text-align: center;" >City</th>  
                                    <th style="text-align: center;" >State</th>  
                                    <th style="text-align: center;" >Membership Date</th>                                                
                                    <th style="text-align: center;" >Status</th>                                                
                                    <th style="text-align: center;" >Access Level</th>                                                
                                    <th style="text-align: center;" >Admin Name</th>                                                
                                    <th style="text-align: center;" >Email Address</th>                                                
                                    <th style="text-align: center;" >Admin ID</th>                                                
                                    <th style="text-align: center;" >Action</th>                                               
                                </tr>
                            </thead>
                            <tbody>
                                
                            
                            <?php
                            $i = 1;
                                foreach ($data as $item)  {?> 
                            
                                    <tr class="odd gradeX">
                                    <td style="text-align: center; text-align: center; vertical-align: middle;"><?=$i?></td>
                                    <td style="text-align: center; text-align: center; vertical-align: middle;"><?=$item->id?></td>                                            
                                    <td style="text-align: center; vertical-align: middle;"><?=$item->name?></td>
                                    <td style="text-align: center; vertical-align: middle;"><?=$item->address1?></td>
                                    <td style="text-align: center; vertical-align: middle;"><?=$item->address2?></td>
                                    <td style="text-align: center; vertical-align: middle;"><?=$item->city?></td>
                                    <td style="text-align: center; vertical-align: middle;"><?=$item->state?></td>
                                    <td style="text-align: center; vertical-align: middle;"><?=explode(" ", $item->membership_date)[0]?></td>
                                    <td style="text-align: center; vertical-align: middle;"><?=($item->status == 1) ? "Active" : "deactive"?></td>
                                    <td style="text-align: center; vertical-align: middle;"><?=$item->access_level?></td> 
                                    <td style="text-align: center; vertical-align: middle;"><?=$item->admin_name?></td> 
                                    <td style="text-align: center; vertical-align: middle;"><?=$item->admin_email?></td> 
                                    <td style="text-align: center; vertical-align: middle;"><?=$item->admin_id_by?></td> 
                                    
                                    <td style="text-align: center;">
                                        <div class="dropdown">
                                           <button class="btn-flat btn-primary dropdown-toggle" 
                                                  type="button" id="dropdownMenu1" data-toggle="dropdown">Action
                                                        <span class="caret"></span>
                                           </button>
                                               <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">                                                                                                              
                                                  <li role="presentation">
                                                  <a role="menuitem" tabindex="-1" onclick = "deleteClick(<?=$item->id?>)">Delete</a>
                                                  </li>

                                               </ul>
                                         </div>                                                                     
                                    </td> 
                                </tr>                                      
                            <?php  $i++;}  ?>
                            
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    
                </div>
                <!-- /.panel-body -->
                
                <div class="panel-footer " style="text-align:center;">                    
                    <input type="button" class=" btn-flat btn-primary dropdown-toggle" onclick="addClick();" value="  Add  ">
                </div>
                                 
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->             
</div>
<!-- /#page-wrapper -->

<script type="text/javascript">
    function showImage(img) {
        
        var src = img.src;
        window.open( src, "_blank", "toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=no, resizable=no, copyhistory=no, titlebar=no, top=100,left=200, width = 256, height = 256");
    }
    
    function addClick() {
            
        location.href = "<?php echo base_url();?>index.php/admin/addClub";         
    }
    
    function deleteClick(id) {
        
        if (confirm("Are you sure want to delete?")) {    
        location.href = "<?php echo base_url();?>"+"index.php/admin/deleteClub/" + id;         
        }
    }
</script>

