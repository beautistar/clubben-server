<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-header">Super admin manage   >  Edit Super Admin    </h4>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    
        <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Information   
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                
                <div class="row">
                    <div class="col-md-7" >
                    
                        <form    role="form" name = "aForm" method="post" action="<?php echo base_url();?>index.php/admin/editSuperConfirm" >
                            
                            <input type="hidden" name = id value="<?=$data->id?>">              
                            <div class="row" style="margin-top: 30px;">
                                <div class="col-lg-2">
                                    User name :     
                                </div>

                                <div class="col-lg-10">
                                    <input type="text" id = "name" class="col-lg-10" name = name placeholder="User Name" value="<?=$data->name?>" required>
                                </div>

                            </div>

                            <div class="row" style="margin-top: 50px;">
                                <div class="col-lg-2">
                                    Email Address :     
                                </div>

                                <div class="col-lg-10">
                                    <input type="text" id = "email" class="col-lg-10" name = email placeholder="Email address" value="<?=$data->email?>" required>
                                </div>

                            </div>
                            
                            <div class="row" style="margin-top: 50px;">
                                <div class="col-lg-2">
                                    Password :      
                                </div>

                                <div class="col-lg-10">
                                    <input class="col-lg-10" id="password" type="password" name = password placeholder="Password" value="<?=$data->password?>" required>
                                </div>
                            </div>
                                                                                                                  
                           <div class=row>
                                <div class="col-lg-11 " style="margin-top: 50px; margin-bottom: 30px;">
                                        <input class="col-lg-12 btn-primary" type=submit value = "Update" >                                           
                                </div>
                            </div>                      
                              
                        </form>                  
                    </div>  
                </div>    
                    
                    
                </div>
                <!-- /.panel-body -->
                
                <div class="panel-footer " style="text-align:center;">                    
                    <input type="hidden">
                </div>                
                
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>     
    
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
        
    
<?php
    if($this->session->flashdata('msg')){
    ?>
    <script>
        alert('<?=$this->session->flashdata('msg')?>');
    </script>
    <?php
    }
        
?>

