<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-header">Staff manage   >  Add New Staff</h4>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    
        <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Information   
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                
                <div class="row">
                    <div class="col-lg-12" >
                    
                        <form    role="form" name = "aForm" method="post" action="<?php echo base_url();?>index.php/admin/addClubOneConfirm" >
                                          
                            <div class="row" style="margin-top: 30px;">
                                <div class="col-lg-1"><strong>Club Name :</strong></div>                                

                                <div class="col-lg-5">
                                    <input type="text" id = "name" class="col-lg-12" name = name placeholder="Club Name" required>
                                </div>
                                
                                <div class="col-lg-1"><strong>Access Level :</strong></div>                                
                                <div class="col-lg-5">
                                    <select name="access_level" class="col-lg-12" required>

                                            <option value="Full">Full</option>
                                            <option value="Lite">Lite</option>
                                        </select>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 50px;">
                                <div class="col-lg-1"><strong>Address 1 : </strong></div>

                                <div class="col-lg-5">
                                    <input type="text" id = "password" class="col-lg-12" name = address1 placeholder="Address 1" required>
                                </div>
                                
                                <div class="col-lg-1"><strong>Admin's name : </strong></div>

                                <div class="col-lg-5">
                                    <input type="text" class="col-lg-12" name = admin_name placeholder="Admin's name" required>
                                </div>

                            </div>
                            
                            
                            <div class="row" style="margin-top: 50px;">
                                <div class="col-lg-1"><strong>Address 2 : </strong></div>

                                <div class="col-lg-5">
                                    <input type="text" class="col-lg-12" name = address2 placeholder="Address 2" required>
                                </div>
                                
                                <div class="col-lg-1"><strong>Email Address</strong></div>

                                <div class="col-lg-5">
                                    <input type="text" class="col-lg-12" name = admin_email placeholder="Email address" required>
                                </div>

                            </div>
                            
                            <div class="row" style="margin-top: 50px;">
                                <div class="col-lg-1"><strong>City : </strong></div>

                                <div class="col-lg-5">
                                    <input type="text" id = "password" class="col-lg-12" name = city placeholder="City" required>
                                </div>
                                
                                <div class="col-lg-1"><strong>Admin ID : </strong></div>

                                <div class="col-lg-5">
                                    <input type="text" class="col-lg-12" name = admin_id placeholder="Admin ID" value="<?=$this->session->userdata('uid')?>" disabled="true" required>
                                </div>

                            </div>
                            
                            <div class="row" style="margin-top: 50px;">
                                <div class="col-lg-1"><strong>State : </strong></div>

                                <div class="col-lg-5">
                                    <select name="state" class="col-lg-12" required>
                                        <?php
                                            foreach ($data as $state) {
                                        ?>
                                        <option value=<?=$state->name?>><?=$state->name?></option>
                                        <?php
                                            }
                                         ?>
                                    </select>
                                </div>
                                
                                <div class="col-lg-1"><strong>Zip code : </strong></div>

                                <div class="col-lg-5">
                                    <input type="text" class="col-lg-12" name = zip_code placeholder="Zip code" required>
                                </div>
                            </div>  
                            
                            <div class="row" style="margin-top: 50px;">
                                <div class="col-lg-1"></div>

                                <div class="col-lg-5">
                                    
                                </div>
                                
                                <div class="col-lg-1"><strong>Password : </strong></div>

                                <div class="col-lg-5">
                                    <input type="text" class="col-lg-12" name = password placeholder="Password" required>
                                </div>
                            </div>                          
                                                                                                                  
                           <div class=row>
                                <div class="col-lg-12" style="margin-top: 50px; margin-bottom: 30px;">
                                        <input class="col-lg-12 btn-primary" type=submit value = "Add" >                                           
                                </div>
                            </div>                      
                              
                        </form>                  
                    </div>  
                </div>
                    
                </div>
                <!-- /.panel-body -->
                
                <div class="panel-footer " style="text-align:center;">                    
                    <input type="hidden">
                </div>                
                
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>     
    
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->

    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });

    function onlyNumber(event){
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        var number_length = document.getElementById("phone_number").value.length;
        if ( (keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) ||  keyID == 8 || keyID == 46 ||  number_length < 11 ) 
            return;
        else
            return false;
    }
    function removeChar(event) {
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        if ( keyID == 8 || keyID == 46 ) 
            return;
        else
            event.target.value = event.target.value.replace(/[^0-9]/g, "");
    }
    
    function valueSelect(myval) {
        var name = document.getElementById('name').value;
        var phone_number = document.getElementById('phone_number').value;
        var device_number = document.getElementById('device_number').value;
        
        if (name.length == 0) name="A";
        if (phone_number.length == 0) phone_number="A";
        if (device_number.length == 0) device_number="A" ;
        location.href = "<?php echo base_url();?>"+"index.php/admin/addTeacherWithRegion/"+myval+'/'+name+'/'+phone_number+'/'+device_number;
//                newWin = window.open("<?php echo base_url();?>"+"admin/updateBranch/"+myval, '_blank', "toolbar=no,location=1,scrollbars=1"
//            + ",width=800,height=650,top=100,left=100");
    }
    </script>    
    
<?php
    if($this->session->flashdata('msg')){
    ?>
    <script>
        alert('<?=$this->session->flashdata('msg')?>');
    </script>
    <?php
    }
        
?>

