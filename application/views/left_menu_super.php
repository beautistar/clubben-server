
<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url();?>">ClubBEN Super Admin</a>
            </div>
            <!-- /.navbar-header -->             

            <ul class="nav navbar-top-links navbar-right" >  
                
                <a href="#"><?=$this->session->userdata('name')?></a>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-message">
                        <li><a href="<?php echo base_url();?>index.php/admin/resetSuperAdmin"><i class="fa fa-user fa-fw"></i>Admin</a>
                        </li>
                                               
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url();?>index.php/admin/logout"><i class="fa fa-sign-out fa-fw"></i>Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">                    
                        <li>
                            <a href="#"><i class="fa fa-road fa-fw"></i>  Manage Super<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url();?>index.php/admin/super"> Super Admins</a>
                                    <!-- /.nav-third-level -->
                                </li>                                                            
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                        <li>
                            <a href="#"><i class="fa fa-road fa-fw"></i>  Manage Clubs<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url();?>index.php/admin/getClub"> Clubs</a>
                                    <!-- /.nav-third-level -->
                                </li>                                                            
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <!--
                        <li>
                            <a href="#"><i class="fa fa-home fa-fw"></i>  Club Admin Manage<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level"> 

                                <li>
                                    <a href="<?php echo base_url();?>index.php/admin/getClubAdmin"> Club Admin</a>
                                    <!-- /.nav-third-level 
                                </li>
                                                                                                  
                            </ul>
                            <!-- /.nav-second-level 
                        </li>
                      -->  
                    
                    <li>
                        <a href="#"><i class="fa fa-exclamation-triangle fa-fw"></i>  Manage Incidents<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo base_url();?>index.php/admin/getIncident"> Incidents</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    
                    <li>
                        <a href="#"><i class="fa fa-users fa-fw"></i>  Manage Patron<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo base_url();?>index.php/admin/getPatronAdmin"> Patrons</a>
                            </li>
                            
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>                    
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
    </nav>