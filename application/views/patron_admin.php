
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-header">Patron</h4>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Patron List   
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th style="text-align: center;" >No</th>
                                    <th style="text-align: center;" >Patron ID</th>
                                    <!--<th style="text-align: center;" >Photo</th>-->
                                    <!--<th style="text-align: center;" >Issuer ID Number</th>-->
                                    <th style="text-align: center;" >License Number</th>
                                    <!--
                                    <th style="text-align: center;" >Expiration Date</th>  
                                    <th style="text-align: center;" >Date of Birth</th>
                                    -->  
                                    <th style="text-align: center;" >Total Visit</th> 
                                    <th style="text-align: center;" >Total Incident</th>
                                    <th style="text-align: center;" >Last Visit</th> 
                                    <!--<th style="text-align: center;" >Created at</th>-->
                                    <!--<th style="text-align: center;" >Action</th>-->                                                
                                </tr>
                            </thead>
                            <tbody>
                                
                            
                            <?php
                            $i = 1;
                                foreach ($data as $item)  {?> 
                            
                                    <tr class="odd gradeX">
                                    <td style="text-align: center; text-align: center; vertical-align: middle;"><?=$i?></td>
                                    <td style="text-align: center; text-align: center; vertical-align: middle;"><?=$item->scan_id.$item->daq?></td>
                                    <!--
                                    <?php
                                    if($item->photo_url){
                                    ?>
                                    <td style="vertical-align: middle; text-align: center; width: 50px; height: 50px;">
                                    <img class="img-rounded img-thumbnail" onclick="showImage(this)" src="<?=$item->photo_url?>" width=200,height=100")" /></td>
                                    <?php
                                       } else {
                                    ?>                                      
                                    <td style="vertical-align: middle; text-align: center; vertical-align: middle;"><img class="img-rounded img-thumbnail" src="<?php echo base_url();?>/frontend/images/avatar_default.jpg" alt=""></td>    
                                    <?php
                                    }
                                    ?>                                    
                                    <td style="text-align: center; text-align: center; vertical-align: middle;"><?=$item->scan_id?></td>-->
                                    <td style="text-align: center; text-align: center; vertical-align: middle;"><?=$item->daq?></td>
                                    <!--
                                    <td style="text-align: center; text-align: center; vertical-align: middle;"><?php echo substr($item->dba, 0, 4); echo "-"; echo substr($item->dba, 4, 2); echo "-"; echo substr($item->dba, 6, 2);?></td>
                                    <td style="text-align: center; text-align: center; vertical-align: middle;"><?php echo substr($item->dbb, 0, 4); echo "-"; echo substr($item->dbb, 4, 2); echo "-"; echo substr($item->dbb, 6, 2);?></td>
                                    -->
                                    <td style="text-align: center; vertical-align: middle;"><?=$item->total_visit?></td>
                                    <td style="text-align: center; vertical-align: middle;"><?=$item->total_incident?></td>
                                    <td style="text-align: center; vertical-align: middle;"><?=explode(" ", $item->last_present_date)[0]?></td>
                                    <!--<td style="text-align: center; vertical-align: middle;"><?=explode(" ", $item->reg_date)[0]?></td>-->
                                    
                                    <!--<td style="text-align: center; vertical-align: middle;">
                                        <div class="dropdown">
                                           <button class="btn-flat btn-primary dropdown-toggle" 
                                                  type="button" id="dropdownMenu1" data-toggle="dropdown">Action
                                                        <span class="caret"></span>
                                           </button>
                                               <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                                  <li role="presentation">
                                                  <a role="menuitem" tabindex="-1" 
                                                        href="<?php echo base_url();?>index.php/admin/editPatron/<?=$item->id?>">Edit                                  
                                                  </a>
                                                  </li>                                                            
                                                  <li role="presentation">
                                                  <a role="menuitem" tabindex="-1" onclick = "deleteClick(<?=$item->id?>)">Delete</a>
                                                  </li>

                                               </ul>
                                         </div>                                                                     
                                    </td>--> 
                                </tr>                                      
                            <?php  $i++;}  ?>
                            
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    
                </div>
                <!-- /.panel-body -->
                
                <div class="panel-footer " style="text-align:center;">                    
                    <input type="button" class=" btn-flat btn-primary dropdown-toggle" onclick="addClick();" value="  Add  ">
                </div>                
                
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->             
</div>
<!-- /#page-wrapper -->

<script type="text/javascript">
    function showImage(img) {
        
        var src = img.src;
        window.open( src, "_blank", "toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=no, resizable=no, copyhistory=no, titlebar=no, top=100,left=200, width = 256, height = 256");
    }
    
    function addClick() {
            
        location.href = "<?php echo base_url();?>index.php/admin/addPatron";         
    }
    
    function deleteClick(id) {
        
        if (confirm("Are you sure want to delete?")) {    
        location.href = "<?php echo base_url();?>"+"index.php/admin/deletePatron/" + id;         
        }
    }
</script>

