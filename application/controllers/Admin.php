<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller{
    
    function __construct(){
        
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->library(array('session', 'form_validation'));
        $this->load->database();
        $this->load->helper(array('cookie', 'url')); 
        $this->load->library('email');
        $this->load->helper('string');
        $this->load->model('admin_model');   
      
    }     
   
    function sessionCheck() {        
        
        if (!isset($this->session->userdata['login'])) {        
                redirect('admin/login');            
        } 
    }    
    
    function index(){
        
        $this->sessionCheck();
        
        if ($this->session->userdata('permission') == 1) {
            
            redirect('admin/super');
            
            
        } else {
            
            redirect('admin/getSecurity');
        }          
    }
    
    function getClub() {
        
        //$data = $this->admin_model->getClubInfo($this->session->userdata('email')); 
        $data = $this->admin_model->getClubInfo(); 
        
        $this->load->view("header");
        $this->load->view("left_menu_super");
        $this->load->view("club", array('data' => $data));
        $this->load->view("footer");        
    }
    
    public function login()

    {
        $getcookival=get_cookie('adminlogin');
        if($getcookival)
        {
            $id = $this -> db -> select('id')-> where('rememberme', $getcookival)-> get('tb_admin');
            if(!$id)
            {
                redirect(base_url());
            }
        }
        else
        {
            if (isset($this->session->userdata['login'])) {
                redirect(base_url());
            }
        }
        $this->load->helper('url');
        $this->load->view('signin'); 
    }
    
    public function pagenotfound()

    {
        $this->load->helper(array('url'));
        $this->load->view('admin/404_notfound');
    }
    
    function logout(){
        
        delete_cookie('adminlogin');
        $data = array('login' => '', 'name' => '', 'enail'=>'', 'uid' => '','permission' => '');
        $this->session->unset_userdata($data);
        
        $this->session->sess_destroy();
        redirect('admin');
    }
    
    public function signinme()
    {
              
        if (isset($this->session->userdata['login'])) {
                redirect(base_url());
            }

        $email = $this->input->post("email");
        $password = $this->input->post("password");

        // form validation
        $this->form_validation->set_rules("email", "Email-ID", "trim|required");
        $this->form_validation->set_rules("password", "Password", "trim|required");
        
        if ($this->form_validation->run() == FALSE)
        {
            // validation fail
            $this->load->view('signin');
        }
        else
        {
            $uresult = $this->admin_model->get_user($email, $password);
            if (count($uresult) > 0 )
            {
                if($uresult[0]->permission < 1)
                {
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Your account not active currently!</div>');
                    redirect('admin/login');
                }
                $remme = $this->input->post("remme");
                if($remme)
                {
                    $seckey=uniqid().time().uniqid();
                    $seckeyen=base64_encode($seckey);
                    set_cookie('adminlogin',$seckeyen,'1209600'); 
                    $this->db->set('rememberme', $seckeyen); 
                    $this->db->where('id', $uresult[0]->id);  
                    
                    $this->db->update('tb_admin');                    
                }
                
                $sess_data = array('login' => TRUE, 'name' => $uresult[0]->name, 'email' => $email, 'uid' => $uresult[0]->id, 'permission'=>$uresult[0]->permission);
                $this->session->set_userdata($sess_data);
                redirect("admin/login");
            }
            else
            {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Wrong Email-ID or Password!</div>');
                redirect('admin/login');
            }
        }
    }
    
    function getSecurity() {
        
        $this->sessionCheck();
        
        $data = $this->admin_model->getSecurityInfo($this->session->userdata('email')); 
        
        $this->load->view("header");
        $this->load->view("left_menu");
        $this->load->view("security", array('data' => $data));
        $this->load->view("footer");
    }
    
    function getPatrons() {
        
        $this->sessionCheck();
        
        $data = $this->admin_model->getPatronsInfo();
        $this->load->view("header");
        $this->load->view("left_menu");
        $this->load->view("patron", array('data' => $data));
        $this->load->view("footer");
    } 
    
    function getIncident() {        
        
        $data = $this->admin_model->getIncidentInfo();
        
        $this->load->view("header");
        $this->load->view("left_menu_super");
        $this->load->view("incident", array('data' => $data));
        $this->load->view("footer");
    }
    
    function allowDispute($id) {
        
        $this->admin_model->allowDispute($id);
        redirect("admin/getIncident");
    }
    
    function declineDispute($id) {
        
        $this->admin_model->declineDispute($id);
        redirect("admin/getIncident");
    }
    
    //////////////////////////////////
    /////// Super Admin part  ////////
    
    
    //super admin menu part
    function super() {
        
        $data = $this->admin_model->getSuperAdminInfo(); 
        
        $this->load->view("header");
        $this->load->view("left_menu_super");
        $this->load->view("super", array('data' => $data));
        $this->load->view("footer");
    }
    
    function addSuperAdmin() {
        
        $this->load->view("header");
        $this->load->view("left_menu_super");
        $this->load->view("add_super_admin");
        $this->load->view("footer"); 
    }
    
    function addSuperConfirm() {
        
        $name = $this->input->post("name");
        $email = $this->input->post("email");
        $password = $this->input->post("password");

        // form validation
        $this->form_validation->set_rules("name", "Name", "trim|required");
        $this->form_validation->set_rules("email", "Email-ID", "trim|valid_email|required");
        $this->form_validation->set_rules("password", "Password", "trim|required");
        
        if ($this->form_validation->run() == FALSE)
        {
            // validation fail
            redirect('admin/addSuperAdmin');
        }
        else
        {
            $name_exist = $this->admin_model->getSuperNameExist($name);
            $email_exist = $this->admin_model->getSuperEmailExist($email);
            
            if ( $name_exist > 0 )
            {
                $this->session->set_flashdata('msg', 'User name already exist!');
                redirect('admin/addSuperAdmin');
            }
            
            if ( $email_exist > 0 )
            {
                $this->session->set_flashdata('msg', 'Email address already exist!');
                redirect('admin/addSuperAdmin');
            }
            
            $this->admin_model->addNewSuper($name, $email, $password);
            redirect('admin/super');
        }                                                              
    }
    
    function editSuperAdmin($id) {
        
        $data = $this->admin_model->getSuperAdminOne($id);
        
        $this->load->view("header");
        $this->load->view("left_menu_super");
        $this->load->view("edit_super_admin", array('data' => $data));
        $this->load->view("footer");
    }
    
    function editSuperConfirm() {
        
        $id = $this->input->post("id");
        $name = $this->input->post("name");
        $email = $this->input->post("email");
        $password = $this->input->post("password");

        // form validation
        $this->form_validation->set_rules("name", "Name", "trim|required");
        $this->form_validation->set_rules("email", "Email-ID", "trim|valid_email|required");
        $this->form_validation->set_rules("password", "Password", "trim|required");
        
        if ($this->form_validation->run() == FALSE)
        {
            // validation fail
            $this->session->set_flashdata('msg', 'Invalid input. try again!');
            redirect('admin/editSuperAdmin/'.$id);
        }
        else
        {
            $name_exist = $this->admin_model->getSuperNameExist($name);
            $email_exist = $this->admin_model->getSuperEmailExist($email);
            
            if ( $name_exist > 0 )
            {
                $this->session->set_flashdata('msg', 'User name already exist!');
                redirect('admin/editSuperAdmin/'.$id);
            }
            
            if ( $email_exist > 0 )
            {
                $this->session->set_flashdata('msg', 'Email address already exist!');
                redirect('admin/editSuperAdmin/'.$id);
            }
            
            $this->admin_model->updateSuperAdmin($id, $name, $email, $password);
            redirect('admin/super');
        }
    }
    
    function deleteSuperAdmin($id) {
        
        $this->admin_model->deleteSuperAdmin($id);
        
        redirect('admin/super');         
    }    
    
    // club admin menu part
    function getClubAdmin() {
        
        $data = $this->admin_model->getClubAdminInfo(); 
        
        $this->load->view("header");
        $this->load->view("left_menu_super");
        $this->load->view("club_admin", array('data' => $data));
        $this->load->view("footer");        
    }
    
    function addClubAdmin() {
        
        $this->load->view("header");
        $this->load->view("left_menu_super");
        $this->load->view("add_club_admin");
        $this->load->view("footer"); 
    }
    
    function addClubConfirm() {
        
        $name = $this->input->post("name");
        $email = $this->input->post("email");
        $password = $this->input->post("password");

        // form validation
        $this->form_validation->set_rules("name", "Name", "trim|required");
        $this->form_validation->set_rules("email", "Email-ID", "trim|valid_email|required");
        $this->form_validation->set_rules("password", "Password", "trim|required");
        
        if ($this->form_validation->run() == FALSE)
        {
            // validation fail
            redirect('admin/addClubAdmin');
        }
        else
        {
            $name_exist = $this->admin_model->getSuperNameExist($name);
            $email_exist = $this->admin_model->getSuperEmailExist($email);
            
            if ( $name_exist > 0 )
            {
                $this->session->set_flashdata('msg', 'User name already exist!');
                redirect('admin/addClubAdmin');
            }
            
            if ( $email_exist > 0 )
            {
                $this->session->set_flashdata('msg', 'Email address already exist!');
                redirect('admin/addClubAdmin');
            }
            
            $this->admin_model->addNewClub($name, $email, $password);
            redirect('admin/getClubAdmin');
        }                                                              
    }
    
    function editClubAdmin($id) {
        
        $data = $this->admin_model->getSuperAdminOne($id);
        
        $this->load->view("header");
        $this->load->view("left_menu_super");
        $this->load->view("edit_club_admin", array('data' => $data));
        $this->load->view("footer");
    }
    
    function editClubConfirm() {
        
        $id = $this->input->post("id");
        $name = $this->input->post("name");
        $email = $this->input->post("email");
        $password = $this->input->post("password");

        // form validation
        $this->form_validation->set_rules("name", "Name", "trim|required");
        $this->form_validation->set_rules("email", "Email-ID", "trim|valid_email|required");
        $this->form_validation->set_rules("password", "Password", "trim|required");
        
        if ($this->form_validation->run() == FALSE)
        {
            // validation fail
            $this->session->set_flashdata('msg', 'Invalid input. try again!');
            redirect('admin/editClubAdmin/'.$id);
        }
        else
        {
            $name_exist = $this->admin_model->getSuperNameExist($name);
            $email_exist = $this->admin_model->getSuperEmailExist($email);
            
            if ( $name_exist > 0 )
            {
                $this->session->set_flashdata('msg', 'User name already exist!');
                redirect('admin/editClubAdmin/'.$id);
            }
            
            if ( $email_exist > 0 )
            {
                $this->session->set_flashdata('msg', 'Email address already exist!');
                redirect('admin/editClubAdmin/'.$id);
            }
            
            $this->admin_model->updateClubAdmin($id, $name, $email, $password);
            redirect('admin/getClubAdmin');
        }
    }
    
    function deleteClubAdmin($id) {
        
        $this->admin_model->deleteSuperAdmin($id);
        
        redirect('admin/getClubAdmin');         
    }
    
    // patron admin part
    
    function getPatronAdmin() {
        
        $this->sessionCheck();
        
        $data = $this->admin_model->getPatronsInfo();
        
        $this->load->view("header");
        $this->load->view("left_menu_super");
        $this->load->view("patron_admin", array('data' => $data));
        $this->load->view("footer");
    }
    
    function addPatron() {
        
        $this->load->view("header");
        $this->load->view("left_menu_super");
        $this->load->view("add_patron");
        $this->load->view("footer"); 
    }
    
    function addPatronConfirm() {
        
        $iin = $this->input->post("iin");
        $daq = $this->input->post("daq");
        $dba = $this->input->post("dba");
        $dbb = $this->input->post("dbb");

        // form validation
        $this->form_validation->set_rules("iin", "IIN", "trim|required");
        $this->form_validation->set_rules("daq", "DAQ", "trim|required");
        $this->form_validation->set_rules("dba", "DBA", "trim|required");
        $this->form_validation->set_rules("dbb", "DBB", "trim|required");
        
        if ($this->form_validation->run() == FALSE)
        {
            // validation fail
            redirect('admin/addPatron');
        }
        else
        {
            $patron_exist = $this->admin_model->getPatronExist($iin, $daq, $dba, $dbb);
            
            if ( $patron_exist > 0 )
            {
                $this->session->set_flashdata('msg', 'Patron already exist!');
                redirect('admin/addPatron');
            }            
            
            $this->admin_model->addNewPatron($iin, $daq, $dba, $dbb);
            redirect('admin/getPatronAdmin');
        } 
    }
    
    function editPatron($id) {
        
        $data = $this->admin_model->getPatronOne($id);
        
        $this->load->view("header");
        $this->load->view("left_menu_super");
        $this->load->view("edit_patron", array('data' => $data));
        $this->load->view("footer"); 
    }
    
    function editPatronConfirm() {
        
        $id = $this->input->post("id");
        $iin = $this->input->post("iin");
        $daq = $this->input->post("daq");
        $dba = $this->input->post("dba");
        $dbb = $this->input->post("dbb");
        
        //print_r($id.$iin.$daq.$dba.$dbb);

        // form validation
        
        $this->form_validation->set_rules("iin", "IIN", "trim|required");
        $this->form_validation->set_rules("daq", "DAQ", "trim|required");
        $this->form_validation->set_rules("dba", "DBA", "trim|required");
        $this->form_validation->set_rules("dbb", "DBB", "trim|required");
        
        if ($this->form_validation->run() == FALSE)
        {
             //validation fail
            redirect('admin/addPatron');
        }
        else
        {
            $patron_exist = $this->admin_model->getPatronExist($iin, $daq, $dba, $dbb);
            
            if ( $patron_exist > 0 )
            {
                $this->session->set_flashdata('msg', 'Patron already exist!');
                redirect('admin/addPatron');
            }            
            
            $this->admin_model->updatePatron($id, $iin, $daq, $dba, $dbb);
            redirect('admin/getPatronAdmin');
        } 
    }
    
    function deletePatron($id) {
        
        $this->admin_model->deletePatron($id);
        
        redirect('admin/getPatronAdmin');         
    }
    
    //2017/06/18 update
    
    function resetSuperAdmin() {
        
        $id = $this->session->userdata['uid'];
        $data = $this->admin_model->getSuperAdminOne($id);
        
        $this->load->view("header");
        $this->load->view("left_menu_super");
        $this->load->view('reset_admin', array('data' => $data));
        $this->load->view("footer"); 
        
    }
    
    function resetSuperPasswordConfirm() {
        
        $id = $this->input->post("id");
        $new_password = $this->input->post("new_password");
        $confirm_password = $this->input->post("confirm_password");

        // form validation
        
        $this->form_validation->set_rules('new_password', 'New Password', 'required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[new_password]');
        
        if ($this->form_validation->run() == FALSE)
        {
            // validation fail
            $this->session->set_flashdata('msg', 'Please confirm password');
            redirect('admin/resetSuperAdmin');
        }
        else
        {
            $this->admin_model->resetSuperPassword($id, $new_password);
            redirect('admin/super');
        } 
    }
    
    /// add & delete patron
    
    function addSecurity() {
        
        $this->load->view("header");
        $this->load->view("left_menu");
        
        $data = $this->admin_model->getClubInfoByMail($this->session->userdata('email'));
        
        $this->load->view("add_security", array('data' => $data));
        $this->load->view("footer"); 
    }
    
    function addSecurityConfirm() {
        
        $email = $this->input->post("email");
        $username = $this->input->post("username");
        $password = $this->input->post("password");
        $permission = $this->input->post("permission");
        $club_id = $this->input->post("club");

        // form validation
        $this->form_validation->set_rules("username", "User Name", "trim|required");
        $this->form_validation->set_rules("email", "Email-ID", "trim|valid_email|required");
        $this->form_validation->set_rules("password", "Password", "trim|required");
        $this->form_validation->set_rules("permission", "Permission", "trim|required");
        $this->form_validation->set_rules("club", "Club selection", "trim|required");
        
        if ($this->form_validation->run() == FALSE)
        {
            // validation fail
            $this->session->set_flashdata('msg', 'Invalid information');
            redirect('admin/addSecurity');
        }
        else
        {
            $security_exist = $this->admin_model->getSecurityExist($email);
            
            if ( $security_exist > 0 )
            {
                $this->session->set_flashdata('msg', 'Security Email already exist!');
                redirect('admin/addSecurity');
            }            
            $club_name = $this->admin_model->getClubName($club_id);
            if(!function_exists('password_hash'))
             $this->load->helper('password');

             $password_hash = password_hash($password, PASSWORD_BCRYPT);
            $this->admin_model->addNewSecurity($username, $email, $password_hash, $permission, $club_id, $club_name);
            redirect('admin/getSecurity');
        } 
    }
    
    function deleteSecurity($id) {
        
        $this->admin_model->deleteSecurity($id);
        
        redirect('admin/getSecurity');         
    }
    
    function resetClubAdmin() {
        
        $this->sessionCheck();
        
        $id = $this->session->userdata['uid'];
        $data = $this->admin_model->getSuperAdminOne($id);
        
        $this->load->view("header");
        $this->load->view("left_menu");
        $this->load->view('reset_club_admin', array('data' => $data));
        $this->load->view("footer"); 
        
    }
    
    function resetClubPasswordConfirm() {
        
        $id = $this->input->post("id");
        $new_password = $this->input->post("new_password");
        $confirm_password = $this->input->post("confirm_password");

        // form validation
        
        $this->form_validation->set_rules('new_password', 'New Password', 'required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[new_password]');
        
        if ($this->form_validation->run() == FALSE)
        {
            // validation fail
            $this->session->set_flashdata('msg', 'Please confirm password');
            redirect('admin/resetClubAdmin');
        }
        else
        {
            $this->admin_model->resetSuperPassword($id, $new_password);
            redirect('admin/getSecurity');
        } 
    }
    
    function addClub() {
        
        $data = $this->admin_model->getState();
        
        $this->load->view("header");
        $this->load->view("left_menu_super");
        $this->load->view("add_club", array('data' => $data));
        $this->load->view("footer"); 
    }
    
    function addClubOneConfirm() {
        
        $name = $this->input->post("name");
        $address1 = $this->input->post('address1');
        $address2 = $this->input->post("address2");
        $city = $this->input->post("city");
        $access_level = $this->input->post("access_level");
        $state = $this->input->post("state");
        $admin_name = $this->input->post("admin_name");
        $admin_email = $this->input->post("admin_email");
        $admin_pwd = $this->input->post("password");

        $club_exist = $this->admin_model->getClubOneNameExist($name);
        
        if ( $club_exist > 0 )
        {
            $this->session->set_flashdata('msg', 'Club Name already exist!');
            redirect('admin/addClub');
        }            
        $admin_id = $this->session->userdata('uid');
        $this->admin_model->addNewClubOne($name, $address1, $address2, $city, $state, $admin_id, $access_level, $admin_name, $admin_email, $admin_pwd);
        redirect('admin/getClub');
    }
    
    function deleteClub($id) {
        
        $this->admin_model->deleteClub($id);
        
        redirect('admin/getClub');         
    }    
   
   
}