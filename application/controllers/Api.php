<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller  {


    function __construct(){

        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('api_model');
        $this->load->library('session');     
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

     private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
     }

     /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

     private function doRespondSuccess($result){

         $this->doRespond(0, $result);
     }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

     private function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
     }
	 
	 function version_test() {
		 
		 echo phpinfo();
	 }
       
     
     public function register($name, $email, $password) {

         $result = array();

         $name = $this->doUrlDecode($name);
         $email = $this->doUrlDecode($email);          
         $password = $this->doUrlDecode($password);

         $usernameExist = $this->api_model->usernameExist($name);
         $emailExist = $this->api_model->emailExist($email);

         if($usernameExist > 0) { // username already exist

             $this->doRespond(101, $result);
             return;
         }

         if ($emailExist > 0) {

             $this->doRespond(102, $result);
             return;
         }
         
         if(!function_exists('password_hash'))
             $this->load->helper('password');

         $password_hash = password_hash($password, PASSWORD_BCRYPT);

         $result['id'] = $this->api_model->register($name, $email, $password_hash);  

         $this->doRespondSuccess($result);

     }
     
     function uploadPhoto() {// baseurl wrong
         
         $result = array();

         $p_id = $_POST['id'];

         $upload_path = "uploadfiles/patron/";  

         $upload_url = base_url()."uploadfiles/patron/";

        // Upload file.

        $image_name = $user_id."_file";

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 4000,
            'max_height' => 3000,
            'file_name' => $image_name.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('file')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $this->api_model->updatePhoto($p_id, $file_url);
            $result['photo_url'] = $file_url;
            $this->doRespondSuccess($result);
            return;

        } else {

            $this->doRespond(103, $result);// upload fail
            return;
        }
     }
     
     function login($email, $password) { 
         
         $result = array();
         $email = $this->doUrlDecode($email);
         $password = $this->doUrlDecode($password);
                           
         $is_exist = $this->api_model->emailExist($email);

         if ($is_exist == 0) {
             $this->doRespond(201, $result);  // Unregistered email.
             return;
         }

         if(!function_exists('password_verify'))
             $this->load->helper('password'); // password_helper.php loading

         $row = $this->db->get_where('tb_user', array('email'=>$email))->row();
         $pwd = $row->password;

         if (!password_verify($password, $pwd)){  // wrong password.

             $this->doRespond(202, $result);
             return;

         } else {
             
             $result['user_info'] = array('idx' => $row->id,
                                          'username' => $row->username,                                          
                                          'club_id' => $row->club_id,
                                          'club_name' => $row->club_name,
                                          'email' => $row->email,
                                          'permission' => $row->permission);                         
         }

         $this->doRespondSuccess($result);  
         
     }
     
     function getEvaluation($user_id, $club_id, $scan_id, $daq, $dba, $dbb) {
                                 
        $result = array();        
        
        $isExist = $this->api_model->patronExist($scan_id, $daq, $club_id);
        
        $p_id = 0;
         
        if ($isExist == 0) {
             
            $p_id = $this->api_model->registerPatron($user_id, $club_id, $scan_id, $daq, $dba, $dbb);
            
        } else {
            
            $p_id = $this->api_model->getPatronId($club_id, $scan_id, $daq);
        }
        
        $result = $this->api_model->getPatronInfo($p_id);
        $this->doRespondSuccess($result);  
     }                                      
     
     function acceptPatronWithPhoto() {
         
         $result = array();
         
         $p_id = $_POST['id'];
         //$result['post_id'] = $p_id;
         $this->api_model->acceptPatron($p_id);         

         $upload_path = "uploadfiles/patron/";  

         $upload_url = base_url()."uploadfiles/patron/";

        // Upload file.

        $image_name = $p_id."_photo";

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 4000,
            'max_height' => 3000,
            'file_name' => $image_name.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('file')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $this->api_model->updatePatronPhoto($p_id, $file_url);
            $result['photo_url'] = $file_url;
            $this->doRespondSuccess($result);
            return;

        } else {

            $this->doRespond(203, $result);// upload fail
            return;
        }
     }
     
     function getPatronByScan($scan_id, $daq, $dba, $dbb) {
                                 
        $result = array();        
        
        $isExist = $this->api_model->patronExist($scan_id, $daq);

        if ($isExist == 0) {
             
            $this->doRespond(204, $result);
            
        } else {
            
            $result['patron_id'] = $this->api_model->getPatronId($scan_id, $daq);
            $result['photo_url'] = $this->api_model->getPatronPhoto($scan_id, $daq);
            $this->doRespondSuccess($result);
        } 
     }
     
     function submitIncident() {
         
         $result = array();
         
         $p_id = $_POST['patron_id'];
         $club_id = $_POST['club_id'];
         //$security_id = 1;
         $p_type = $_POST['type'];
         $incident_id = $this->doUrlDecode($_POST['incident_id']);
         $p_description = $this->doUrlDecode($_POST['description']);
         
        $file_url = $this->db->where('id', $p_id)->get('tb_patron')->row()->photo_url;
        $this->api_model->createIncident($p_id, $p_type, $p_description, $file_url, $club_id, $incident_id);
        //$result['photo_url'] = $file_url;
        $this->doRespondSuccess($result);
       
     }
     
     function getPatrons($user_id, $club_id, $page_index) {
         
         $result = array();
         
         $result['patron_info'] = $this->api_model->getPatrons($user_id, $club_id, $page_index);
         
         $this->doRespondSuccess($result);
     }
     
     function getDisputeIncidentListByScan($iin, $daq, $dba, $dbb) {
                                 
        $result = array();        
        
        $p_id = $this->api_model->patronExistWith($iin, $daq);

        if ($p_id == 0) {
             
            $this->doRespond(204, $result);
            
        } else {
            
            $result = $this->api_model->getPatronInfo($p_id);
            $this->doRespondSuccess($result);
        } 
     }
     
     function submitDispute() {
         
         $result = array();
         
         $p_id = $_POST['incident_id'];
         $p_comment = $this->doUrlDecode($_POST['comment']);
         
         // if request already submitted,
         $state = $this->api_model->getSubmitState($p_id);
         
         if ($state > 0) {
             
             $this->doRespond(205, $result);
         } else {
             
             $this->api_model->submitDispute($p_id, $p_comment);
         
             $this->doRespondSuccess($result);
         }        
     }
     
     ///test
     
     function getState() {
         
         $result = array();
         
         $query = $this->db->get('tb_states');
         
         $this->doRespondSuccess($query->result());
                
     }
     
     function getRecords() {
         
         $result = array();
         
         $patron_number = $this->db->get('tb_patron')->num_rows();
         $security_number = $this->db->get('tb_user')->num_rows();
         $incident_number = $this->db->get('tb_incident')->num_rows();
         
         $result = array('patron_number' => $patron_number,
                         'security_number' => $security_number,
                         'incident_number' => $incident_number);
                         
         $this->doRespondSuccess($result);
     }
                          
}

?>
