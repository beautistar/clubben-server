-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 02, 2017 at 03:00 AM
-- Server version: 5.5.54-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `clubben`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE IF NOT EXISTS `tb_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission` int(2) NOT NULL,
  `create_at` datetime NOT NULL,
  `rememberme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id`, `name`, `email`, `password`, `permission`, `create_at`, `rememberme`) VALUES
(1, 'club admin', 'admin@test.com', '123456', 2, '2017-05-21 20:35:08', 'NTkyMzJjNDhhNTljMzE0OTU0NzczMjA1OTIzMmM0OGE1YTZh'),
(2, 'Super admin', 'super@admin.com', '123456', 1, '2017-05-23 09:09:06', 'NTk4MjhjZjVkNmNlMjE1MDE3Mjc5ODk1OTgyOGNmNWQ2Y2Vj'),
(3, 'Super new', 'super1@user.com', '123456', 1, '2017-05-24 04:07:44', ''),
(4, 'super2', 'super2@gmail.com', '123456', 1, '2017-05-24 04:07:47', ''),
(7, 'Club Admin 2', 'club2@admin.com', '123456', 2, '2017-05-24 08:42:02', ''),
(9, 'TestClub2', 'TestClub2@admit.com', '123456', 2, '2017-06-18 23:41:18', ''),
(10, 'j smooth', 'jsmooth@test.com', 'j', 1, '2017-07-20 20:39:02', ''),
(11, 'Admin2', 'admin2@test.com', '2', 2, '2017-09-01 01:54:12', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_club`
--

CREATE TABLE IF NOT EXISTS `tb_club` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `membership_date` datetime NOT NULL,
  `status` int(2) NOT NULL,
  `access_level` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `admin_id_by` int(11) NOT NULL,
  `admin_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `admin_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tb_club`
--

INSERT INTO `tb_club` (`id`, `name`, `address1`, `address2`, `city`, `state`, `membership_date`, `status`, `access_level`, `admin_id_by`, `admin_email`, `admin_name`) VALUES
(1, 'John Club', 'Log Angelse', 'US', 'Los city', 'califonia', '2017-05-22 09:56:54', 1, 'Full', 2, 'super@admin.com', 'Super admin'),
(2, 'Marin Club', 'Log Angels', 'Califonia', 'Wosington', 'Ohio', '2017-06-20 03:31:32', 1, 'Full', 1, 'admin@test.com', 'club admin'),
(5, 'Test Club2', 'Ad1', 'N/A', 'Home', 'Georgia', '2017-07-24 12:21:11', 1, 'Full', 2, 'super@admin.com', 'Super admin'),
(6, 'Test club', 'test', 'add2', 'dummy city', 'Indiana', '2017-08-03 08:35:47', 1, 'Full', 2, 'super@admin.com', 'Super admin'),
(7, 'Test Club 3', '1 Place St', 'Suite A', 'A Town', 'Georgia', '2017-08-04 08:15:57', 1, 'Full', 10, 'jsmooth@test.com', 'j smooth'),
(8, 'DumyClub', 'Dumy address 1', 'Dummy address 2', 'dummy city', 'Indiana', '2017-08-07 01:39:00', 1, 'Lite', 2, 'jsmooth@test.com', 'j smith'),
(9, 'Club 2', '2nd St', 'na', 'here', 'Delaware', '2017-09-01 01:54:12', 1, 'Full', 2, 'admin2@test.com', 'Admin2');

-- --------------------------------------------------------

--
-- Table structure for table `tb_incident`
--

CREATE TABLE IF NOT EXISTS `tb_incident` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `incident_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `patron_id` int(11) NOT NULL,
  `incident_pic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `incident_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `reg_date` date NOT NULL,
  `dispute_request_state` int(2) NOT NULL DEFAULT '0' COMMENT '1:Requested, 2:Allowed, -1:declined',
  `dispute_comment` text COLLATE utf8_unicode_ci NOT NULL,
  `action_date` date NOT NULL,
  `club_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `tb_incident`
--

INSERT INTO `tb_incident` (`id`, `incident_id`, `patron_id`, `incident_pic`, `description`, `incident_type`, `reg_date`, `dispute_request_state`, `dispute_comment`, `action_date`, `club_id`) VALUES
(2, '1-20170821-1', 16, 'http://54.148.154.173/uploadfiles/patron/16_photo14979347230.jpg', 'uncooperative', '1,2,3', '2017-06-20', 0, '', '0000-00-00', 1),
(3, '1-20170821-1', 14, 'http://54.148.154.173/uploadfiles/patron/14_photo14979346308.jpg', 'test associate functionality, uncooperative', '1,2,3', '2017-06-20', -1, 'demonstration of dispute functionality', '2017-09-01', 1),
(4, '1-20170821-1', 13, 'http://54.148.154.173/uploadfiles/patron/13_photo14979345209.jpg', 'test associate functionality, soliciting', '1,2,3', '2017-06-20', 0, '', '0000-00-00', 1),
(5, '1-20170821-1', 10, 'http://54.148.154.173/uploadfiles/patron/10_photo14979344017.jpg', 'test associate functionality, intoxicated', '1,2,3', '2017-06-20', 0, '', '0000-00-00', 1),
(6, '1-20170821-1', 19, 'http://54.148.154.173/uploadfiles/patron/19_photo14979348633.jpg', 'Armed, soliciting, sexual', '1,2,3', '2017-07-20', 0, '', '0000-00-00', 3),
(7, '1-20170821-1', 18, 'http://54.148.154.173/uploadfiles/patron/18_photo14979348239.jpg', 'Armed, soliciting, intoxicated', '1,2,3', '2017-07-20', 0, '', '0000-00-00', 3),
(8, '1-20170821-1', 16, 'http://54.148.154.173/uploadfiles/patron/16_photo14979347230.jpg', 'Violent, intoxicated, sexual', '1,2,3', '2017-08-01', 0, '', '0000-00-00', 3),
(9, '1-20170821-1', 13, 'http://54.148.154.173/uploadfiles/patron/13_photo14979345209.jpg', 'Violent, intoxicated, sexual, armed', '1,2,3', '2017-08-01', 0, '', '0000-00-00', 3),
(10, '1-20170821-1', 9, 'http://54.148.154.173/uploadfiles/patron/9_photo14979343674.jpg', 'Violent', '1,2,3', '2017-08-30', 0, '', '0000-00-00', 1),
(11, '1-20170821-1', 12, 'http://54.148.154.173/uploadfiles/patron/12_photo14979344745.jpg', 'Violent', '1,2,3', '2017-08-30', 0, '', '0000-00-00', 1),
(12, '1-20170830-1', 15, 'http://54.148.154.173/uploadfiles/patron/15_photo14979346829.jpg', 'Violent', '1,2,3', '2017-08-30', 0, '', '0000-00-00', 1),
(13, '1-20170831-6', 17, 'http://54.148.154.173/uploadfiles/patron/17_photo14979347721.jpg', 'Violent, sexual', '1,2,3', '2017-08-31', 0, '', '0000-00-00', 1),
(14, '1-20170831-6', 14, 'http://54.148.154.173/uploadfiles/patron/14_photo14979346308.jpg', 'Violent, sexual', '1,2,3', '2017-08-31', 0, '', '0000-00-00', 1),
(15, '1-20170831-6', 11, 'http://54.148.154.173/uploadfiles/patron/11_photo14979344396.jpg', 'Violent, sexual', '1,2,3', '2017-08-31', 0, '', '0000-00-00', 1),
(16, '1-20170901-1', 22, 'http://54.148.154.173/uploadfiles/patron/20_photo15009108382.jpg', 'Violent, intoxicated, sexual, soliciting', '1,2,3', '2017-08-31', 0, '', '0000-00-00', 1),
(17, '1-20170901-1', 23, 'http://54.148.154.173/uploadfiles/patron/20_photo15009108382.jpg', 'Violent, intoxicated, sexual, soliciting', '1,2,3', '2017-08-31', 0, '', '0000-00-00', 1),
(18, '1-20170901-2', 22, 'http://54.148.154.173/uploadfiles/patron/20_photo15009108382.jpg', 'Intoxicated, sexual', '1,2,3', '2017-08-31', 0, '', '0000-00-00', 1),
(19, '1-20170901-1', 26, 'http://54.148.154.173/uploadfiles/patron/26_photo15042079827.jpg', 'Armed', '1,2,3', '2017-08-31', 0, '', '0000-00-00', 1),
(20, '1-20170901-1', 26, 'http://54.148.154.173/uploadfiles/patron/26_photo15042079827.jpg', 'Soliciting, sexual, intoxicated', '1,2,3', '2017-08-31', 0, '', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_patron`
--

CREATE TABLE IF NOT EXISTS `tb_patron` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `club_id` int(11) NOT NULL,
  `scan_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `daq` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `dba` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `dbb` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `total_visit` int(11) NOT NULL,
  `photo_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_present_date` datetime NOT NULL,
  `reg_date` datetime NOT NULL,
  `dispute_request_state` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=27 ;

--
-- Dumping data for table `tb_patron`
--

INSERT INTO `tb_patron` (`id`, `user_id`, `club_id`, `scan_id`, `daq`, `dba`, `dbb`, `total_visit`, `photo_url`, `last_present_date`, `reg_date`, `dispute_request_state`) VALUES
(1, 1, 1, '636033', '9701741', '20170716', '19900404', 1, 'http://54.148.154.173/uploadfiles/patron/1_photo14979338930.jpg', '2017-06-20 04:44:53', '2017-06-20 04:44:18', 0),
(2, 1, 1, '636056', '3291742', '20170717', '19900405', 1, 'http://54.148.154.173/uploadfiles/patron/2_photo14979339750.jpg', '2017-06-20 04:46:15', '2017-06-20 04:45:11', 0),
(3, 1, 1, '636047', '9701743', '20170718', '19900406', 1, 'http://54.148.154.173/uploadfiles/patron/3_photo14979340123.jpg', '2017-06-20 04:46:52', '2017-06-20 04:46:30', 0),
(4, 1, 1, '636022', '9701744', '20170719', '19900407', 1, 'http://54.148.154.173/uploadfiles/patron/4_photo14979340731.jpg', '2017-06-20 04:47:53', '2017-06-20 04:47:15', 0),
(5, 1, 1, '636046', '9701745', '20170720', '19900408', 1, 'http://54.148.154.173/uploadfiles/patron/5_photo14979341781.jpg', '2017-06-20 04:49:38', '2017-06-20 04:48:57', 0),
(6, 1, 1, '636022', '9701746', '20170721', '19900409', 1, 'http://54.148.154.173/uploadfiles/patron/6_photo14979342231.jpg', '2017-06-20 04:50:23', '2017-06-20 04:49:56', 0),
(7, 1, 1, '636015', '9701748', '20170723', '19900411', 1, 'http://54.148.154.173/uploadfiles/patron/7_photo14979342818.jpg', '2017-06-20 04:51:21', '2017-06-20 04:50:56', 0),
(8, 1, 1, '636058', '9701750', '20170725', '19900413', 2, 'http://54.148.154.173/uploadfiles/patron/8_photo15042321891.jpg', '2017-09-01 02:16:29', '2017-06-20 04:51:34', 0),
(9, 1, 1, '636005', '9701752', '20170727', '19900415', 1, 'http://54.148.154.173/uploadfiles/patron/9_photo14979343674.jpg', '2017-06-20 04:52:47', '2017-06-20 04:52:18', 0),
(10, 1, 1, '636021', '9701754', '20170411', '19900417', 1, 'http://54.148.154.173/uploadfiles/patron/10_photo14979344017.jpg', '2017-06-20 04:53:21', '2017-06-20 04:52:59', 0),
(11, 1, 1, '636009', '9701756', '20170413', '19900419', 1, 'http://54.148.154.173/uploadfiles/patron/11_photo14979344396.jpg', '2017-06-20 04:53:59', '2017-06-20 04:53:33', 0),
(12, 1, 1, '636023', '9701758', '20170415', '19900421', 1, 'http://54.148.154.173/uploadfiles/patron/12_photo14979344745.jpg', '2017-06-20 04:54:34', '2017-06-20 04:54:12', 0),
(13, 1, 1, '636033', '9701760', '20170417', '19900423', 1, 'http://54.148.154.173/uploadfiles/patron/13_photo14979345209.jpg', '2017-06-20 04:55:20', '2017-06-20 04:54:51', 0),
(14, 1, 1, '636045', '9701747', '20170722', '19900410', 1, 'http://54.148.154.173/uploadfiles/patron/14_photo14979346308.jpg', '2017-06-20 04:57:10', '2017-06-20 04:55:47', 0),
(15, 1, 1, '636025', '9701749', '20170724', '19900412', 2, 'http://54.148.154.173/uploadfiles/patron/15_photo15042321058.jpg', '2017-09-01 02:15:05', '2017-06-20 04:57:39', 0),
(16, 1, 1, '604428', '9701751', '20170726', '19900414', 1, 'http://54.148.154.173/uploadfiles/patron/16_photo14979347230.jpg', '2017-06-20 04:58:43', '2017-06-20 04:58:15', 0),
(17, 1, 1, '636014', '9701753', '20170410', '19900416', 2, 'http://54.148.154.173/uploadfiles/patron/17_photo15042323361.jpg', '2017-09-01 02:18:56', '2017-06-20 04:58:57', 0),
(18, 1, 1, '636051', '9701755', '20170412', '19900418', 1, 'http://54.148.154.173/uploadfiles/patron/18_photo14979348239.jpg', '2017-06-20 05:00:23', '2017-06-20 04:59:59', 0),
(19, 1, 1, '636001', '9701757', '20170414', '19900420', 2, 'http://54.148.154.173/uploadfiles/patron/19_photo15009109392.jpg', '2017-07-24 15:42:19', '2017-06-20 05:00:37', 0),
(20, 1, 1, '636046', '9701759', '20170416', '19900422', 6, 'http://54.148.154.173/uploadfiles/patron/20_photo15042325518.jpg', '2017-09-01 02:22:31', '2017-06-20 05:01:15', 0),
(21, 1, 1, '100930', '999000680', '20031212', '19701212', 1, 'http://54.148.154.173/uploadfiles/patron/21_photo15043201718.jpg', '2017-09-02 02:42:51', '2017-07-18 01:44:47', 0),
(22, 1, 1, '636033', '6801813', '06172021', '02051980', 0, 'http://54.148.154.173/uploadfiles/patron/20_photo15009108382.jpg', '2017-08-31 18:29:49', '2017-07-20 00:29:49', 0),
(23, 1, 1, '636055', '059329997', '09022017', '02051980', 0, 'http://54.148.154.173/uploadfiles/patron/20_photo15009108382.jpg', '2017-08-31 19:08:31', '2017-07-20 19:08:31', 0),
(24, 3, 1, '360290', '9646267', '20040707', '19820707', 1, 'http://54.148.154.173/uploadfiles/patron/24_photo15042078177.jpg', '2017-08-31 19:30:17', '2017-08-31 19:24:11', 0),
(25, 1, 1, '636002', '99988801', '20050122', '19550122', 0, '', '2017-08-31 19:32:19', '2017-08-31 19:32:19', 0),
(26, 1, 1, '636041', '0001540', '20001125', '19791125', 2, 'http://54.148.154.173/uploadfiles/patron/26_photo15042657126.jpg', '2017-09-01 11:35:12', '2017-08-31 19:32:41', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_states`
--

CREATE TABLE IF NOT EXISTS `tb_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `iin` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=65 ;

--
-- Dumping data for table `tb_states`
--

INSERT INTO `tb_states` (`id`, `name`, `iin`) VALUES
(1, 'Alabama', '636033'),
(2, 'Alaska', '636059'),
(3, 'Arizona', '636026'),
(4, 'Arkansas', '636021'),
(5, 'British Columbia ', ' 636028'),
(6, 'California', '636014'),
(7, 'Coahuila', '636056'),
(8, 'Colorado', '636020'),
(9, 'Connecticut', '636006'),
(10, 'Delaware', '636011'),
(11, 'District of Columbia', '636043'),
(12, 'Florida', '636010'),
(13, 'Georgia', '636055'),
(14, 'Guam', '636019'),
(15, 'Hawaii', '636047'),
(16, 'Hidalgo', '636057'),
(17, 'Idaho', '636050'),
(18, 'Illinois', '636035'),
(19, 'Indiana', '636037'),
(20, 'Iowa', '636018'),
(21, 'Kansas', '636022'),
(22, 'Kentucky', '636046'),
(23, 'Louisiana', '636007'),
(24, 'Maine', '636041'),
(25, 'Manitoba', '636048'),
(26, 'Maryland', '636003'),
(27, 'Massachusetts', '636002'),
(28, 'Michigan', '636032'),
(29, 'Minnesota', '636038'),
(30, 'Mississippi', '636051'),
(31, 'Missouri', '636030'),
(32, 'Montana', '636008'),
(33, 'Nebraska', '636054'),
(34, 'Nevada', '636049'),
(35, 'New Brunswick', '636017'),
(36, 'New Hampshire', '636039'),
(37, 'New Jersey ', '636036'),
(38, 'New Mexico', '636009'),
(39, 'New York ', '636001'),
(40, 'Newfoundland', '636016'),
(41, 'North Carolina', '636004'),
(42, 'North Dakota', '636034'),
(43, 'Nova Scotia', '636013'),
(44, 'Ohio', '636023'),
(45, 'Oklahoma', '636058'),
(46, 'Ontario', '636012'),
(47, 'Oregon', '636029'),
(48, 'Pennsylvania', '636025'),
(49, 'Prince Edward Island', '604425'),
(50, 'Rhode Island', '636052'),
(51, 'Saskatchewan', '636044'),
(52, 'South Carolina', '636005'),
(53, 'South Dakota', '636042'),
(54, 'State Dept (USA)', '636027'),
(55, 'Tennessee', '636053'),
(56, 'Texas', '636015'),
(57, 'US Virgin Islands', '636062'),
(58, 'Utah', '636040'),
(59, 'Vermont', '636024'),
(60, 'Virginia', '636000'),
(61, 'Washington', '636045'),
(62, 'West Virginia', '636061'),
(63, 'Wisconsin', '636031'),
(64, 'Wyoming', '636060');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE IF NOT EXISTS `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission` int(2) NOT NULL,
  `rememberme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `club_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `club_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `club_admin_id` int(11) NOT NULL,
  `reg_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `username`, `email`, `password`, `permission`, `rememberme`, `club_id`, `club_name`, `club_admin_id`, `reg_date`) VALUES
(1, 'security', 'security@test.com', '$2y$10$WjF5/v34ajmJxH8dEhyA8ua11DNdD3YwL9dumy241DxHBgrWJuBQu', 2, '', '1', 'John Club', 1, '2017-05-05 10:38:14'),
(3, 'admission', 'admission@test.com', '$2y$10$v6nh5uINX5EHOuVC2c/mT.Uh.q3uKapblODhVchPDYx5ndBjNShIK', 3, '', '1', 'John Club', 1, '2017-05-06 01:45:06'),
(5, 'security1', 'sec1@user.com', '$2y$10$tmM1Uua6ljRFCIDAJGPejenerR8uafOqjS8an17ToQ.s0M4xiZmIm', 2, '', '1', 'John Club', 1, '2017-06-19 18:42:38'),
(6, 'sec-test', 'sec@test.com', '$2y$10$8/Dy7H198rsmUHerOG9cTe3CTmcUsv0OFRA7dCaZsSYSeS7NAJ3Qq', 2, '', '5', 'Test Club2', 1, '2017-07-24 15:58:23'),
(7, 'new stuff-1', 'new@stuff1.com', '$2y$10$1zD5j8sPd26tR.nrk7Z9rOx36Lh/tcNfo2A/xS6U9slOsfA72Frv6', 2, '', '2', 'Marin Club', 1, '2017-08-31 11:20:38'),
(8, 'admit2', 'admit2@test.com', '$2y$10$SoF1UeCGLysWktFMez45cOafC2oONLFYV0GEYdaHrJzqR8p29.7KK', 3, '', '9', 'Club 2', 11, '2017-09-01 01:59:48'),
(9, 'Sec2', 'sec2@test.com', '$2y$10$fGQefFN.TilbE7rWUtnJhOfHou3gQQOCbWnRwqq1fz3LTZwp69BgO', 2, '', '9', 'Club 2', 11, '2017-09-01 02:00:21');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
