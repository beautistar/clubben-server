/*
SQLyog Ultimate v11.3 (64 bit)
MySQL - 5.5.54-0ubuntu0.14.04.1 : Database - clubben
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`clubben` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `clubben`;

/*Table structure for table `tb_admin` */

DROP TABLE IF EXISTS `tb_admin`;

CREATE TABLE `tb_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission` int(2) NOT NULL,
  `create_at` datetime NOT NULL,
  `rememberme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_admin` */

insert  into `tb_admin`(`id`,`name`,`email`,`password`,`permission`,`create_at`,`rememberme`) values (1,'club admin','admin@test.com','123456',2,'2017-05-21 20:35:08','NTkyMzJjNDhhNTljMzE0OTU0NzczMjA1OTIzMmM0OGE1YTZh'),(2,'Super admin','super@admin.com','123456',1,'2017-05-23 09:09:06','NTk4MjhjZjVkNmNlMjE1MDE3Mjc5ODk1OTgyOGNmNWQ2Y2Vj'),(3,'Super new','super1@user.com','123456',1,'2017-05-24 04:07:44',''),(4,'super2','super2@gmail.com','123456',1,'2017-05-24 04:07:47',''),(7,'Club Admin 2','club2@admin.com','123456',2,'2017-05-24 08:42:02',''),(9,'TestClub2','TestClub2@admit.com','123456',2,'2017-06-18 23:41:18',''),(10,'j smooth','jsmooth@test.com','j',1,'2017-07-20 20:39:02','');

/*Table structure for table `tb_club` */

DROP TABLE IF EXISTS `tb_club`;

CREATE TABLE `tb_club` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `membership_date` datetime NOT NULL,
  `status` int(2) NOT NULL,
  `access_level` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `admin_id_by` int(11) NOT NULL,
  `admin_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `admin_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_club` */

insert  into `tb_club`(`id`,`name`,`address1`,`address2`,`city`,`state`,`membership_date`,`status`,`access_level`,`admin_id_by`,`admin_email`,`admin_name`) values (1,'John Club','Log Angelse','US','Los city','califonia','2017-05-22 09:56:54',1,'Full',2,'super@admin.com','Super admin'),(2,'Marin Club','Log Angels','Califonia','Wosington','Ohio','2017-06-20 03:31:32',1,'Full',1,'admin@test.com','club admin'),(5,'Test Club2','Ad1','N/A','Home','Georgia','2017-07-24 12:21:11',1,'Full',2,'super@admin.com','Super admin'),(6,'Test club','test','add2','dummy city','Indiana','2017-08-03 08:35:47',1,'Full',2,'super@admin.com','Super admin'),(7,'Test Club 3','1 Place St','Suite A','A Town','Georgia','2017-08-04 08:15:57',1,'Full',10,'jsmooth@test.com','j smooth'),(8,'DumyClub','Dumy address 1','Dummy address 2','dummy city','Indiana','2017-08-07 01:39:00',1,'Lite',2,'jsmooth@test.com','j smith');

/*Table structure for table `tb_incident` */

DROP TABLE IF EXISTS `tb_incident`;

CREATE TABLE `tb_incident` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patron_id` int(11) NOT NULL,
  `incident_pic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `incident_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `reg_date` date NOT NULL,
  `dispute_request_state` int(2) NOT NULL DEFAULT '0' COMMENT '1:Requested, 2:Allowed, -1:declined',
  `dispute_comment` text COLLATE utf8_unicode_ci NOT NULL,
  `action_date` date NOT NULL,
  `club_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_incident` */

insert  into `tb_incident`(`id`,`patron_id`,`incident_pic`,`description`,`incident_type`,`reg_date`,`dispute_request_state`,`dispute_comment`,`action_date`,`club_id`) values (2,16,'http://54.148.154.173/uploadfiles/patron/16_photo14979347230.jpg','uncooperative','1,2,3','2017-06-20',0,'','0000-00-00',1),(3,14,'http://54.148.154.173/uploadfiles/patron/14_photo14979346308.jpg','test associate functionality, uncooperative','1,2,3','2017-06-20',1,'demonstration of dispute functionality','0000-00-00',1),(4,13,'http://54.148.154.173/uploadfiles/patron/13_photo14979345209.jpg','test associate functionality, soliciting','1,2,3','2017-06-20',0,'','0000-00-00',1),(5,10,'http://54.148.154.173/uploadfiles/patron/10_photo14979344017.jpg','test associate functionality, intoxicated','1,2,3','2017-06-20',0,'','0000-00-00',1),(6,19,'http://54.148.154.173/uploadfiles/patron/19_photo14979348633.jpg','Armed, soliciting, sexual','1,2,3','2017-07-20',0,'','0000-00-00',3),(7,18,'http://54.148.154.173/uploadfiles/patron/18_photo14979348239.jpg','Armed, soliciting, intoxicated','1,2,3','2017-07-20',0,'','0000-00-00',3),(8,16,'http://54.148.154.173/uploadfiles/patron/16_photo14979347230.jpg','Violent, intoxicated, sexual','1,2,3','2017-08-01',0,'','0000-00-00',3),(9,13,'http://54.148.154.173/uploadfiles/patron/13_photo14979345209.jpg','Violent, intoxicated, sexual, armed','1,2,3','2017-08-01',0,'','0000-00-00',3);

/*Table structure for table `tb_patron` */

DROP TABLE IF EXISTS `tb_patron`;

CREATE TABLE `tb_patron` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scan_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `daq` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `dba` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `dbb` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `total_visit` int(11) NOT NULL,
  `photo_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_present_date` datetime NOT NULL,
  `reg_date` datetime NOT NULL,
  `dispute_request_state` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_patron` */

insert  into `tb_patron`(`id`,`scan_id`,`daq`,`dba`,`dbb`,`total_visit`,`photo_url`,`last_present_date`,`reg_date`,`dispute_request_state`) values (1,'636033','9701741','20170716','19900404',1,'http://54.148.154.173/uploadfiles/patron/1_photo14979338930.jpg','2017-06-20 04:44:53','2017-06-20 04:44:18',0),(2,'636056','3291742','20170717','19900405',1,'http://54.148.154.173/uploadfiles/patron/2_photo14979339750.jpg','2017-06-20 04:46:15','2017-06-20 04:45:11',0),(3,'636047','9701743','20170718','19900406',1,'http://54.148.154.173/uploadfiles/patron/3_photo14979340123.jpg','2017-06-20 04:46:52','2017-06-20 04:46:30',0),(4,'636022','9701744','20170719','19900407',1,'http://54.148.154.173/uploadfiles/patron/4_photo14979340731.jpg','2017-06-20 04:47:53','2017-06-20 04:47:15',0),(5,'636046','9701745','20170720','19900408',1,'http://54.148.154.173/uploadfiles/patron/5_photo14979341781.jpg','2017-06-20 04:49:38','2017-06-20 04:48:57',0),(6,'636022','9701746','20170721','19900409',1,'http://54.148.154.173/uploadfiles/patron/6_photo14979342231.jpg','2017-06-20 04:50:23','2017-06-20 04:49:56',0),(7,'636015','9701748','20170723','19900411',1,'http://54.148.154.173/uploadfiles/patron/7_photo14979342818.jpg','2017-06-20 04:51:21','2017-06-20 04:50:56',0),(8,'636058','9701750','20170725','19900413',1,'http://54.148.154.173/uploadfiles/patron/8_photo14979343255.jpg','2017-06-20 04:52:05','2017-06-20 04:51:34',0),(9,'636005','9701752','20170727','19900415',1,'http://54.148.154.173/uploadfiles/patron/9_photo14979343674.jpg','2017-06-20 04:52:47','2017-06-20 04:52:18',0),(10,'636021','9701754','20170411','19900417',1,'http://54.148.154.173/uploadfiles/patron/10_photo14979344017.jpg','2017-06-20 04:53:21','2017-06-20 04:52:59',0),(11,'636009','9701756','20170413','19900419',1,'http://54.148.154.173/uploadfiles/patron/11_photo14979344396.jpg','2017-06-20 04:53:59','2017-06-20 04:53:33',0),(12,'636023','9701758','20170415','19900421',1,'http://54.148.154.173/uploadfiles/patron/12_photo14979344745.jpg','2017-06-20 04:54:34','2017-06-20 04:54:12',0),(13,'636033','9701760','20170417','19900423',1,'http://54.148.154.173/uploadfiles/patron/13_photo14979345209.jpg','2017-06-20 04:55:20','2017-06-20 04:54:51',0),(14,'636045','9701747','20170722','19900410',1,'http://54.148.154.173/uploadfiles/patron/14_photo14979346308.jpg','2017-06-20 04:57:10','2017-06-20 04:55:47',0),(15,'636025','9701749','20170724','19900412',1,'http://54.148.154.173/uploadfiles/patron/15_photo14979346829.jpg','2017-06-20 04:58:02','2017-06-20 04:57:39',0),(16,'604428','9701751','20170726','19900414',1,'http://54.148.154.173/uploadfiles/patron/16_photo14979347230.jpg','2017-06-20 04:58:43','2017-06-20 04:58:15',0),(17,'636014','9701753','20170410','19900416',1,'http://54.148.154.173/uploadfiles/patron/17_photo14979347721.jpg','2017-06-20 04:59:32','2017-06-20 04:58:57',0),(18,'636051','9701755','20170412','19900418',1,'http://54.148.154.173/uploadfiles/patron/18_photo14979348239.jpg','2017-06-20 05:00:23','2017-06-20 04:59:59',0),(19,'636001','9701757','20170414','19900420',2,'http://54.148.154.173/uploadfiles/patron/19_photo15009109392.jpg','2017-07-24 15:42:19','2017-06-20 05:00:37',0),(20,'636046','9701759','20170416','19900422',4,'http://54.148.154.173/uploadfiles/patron/20_photo15009108382.jpg','2017-07-24 15:40:38','2017-06-20 05:01:15',0),(21,'100930','999000680','20031212','19701212',0,'','2017-07-18 01:44:47','2017-07-18 01:44:47',0),(22,'636033','6801813','06172021','02051980',0,'','2017-07-20 00:29:49','2017-07-20 00:29:49',0),(23,'636055','059329997','09022017','02051980',0,'','2017-07-20 19:08:31','2017-07-20 19:08:31',0);

/*Table structure for table `tb_states` */

DROP TABLE IF EXISTS `tb_states`;

CREATE TABLE `tb_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `iin` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_states` */

insert  into `tb_states`(`id`,`name`,`iin`) values (1,'Alabama','636033'),(2,'Alaska','636059'),(3,'Arizona','636026'),(4,'Arkansas','636021'),(5,'British Columbia ',' 636028'),(6,'California','636014'),(7,'Coahuila','636056'),(8,'Colorado','636020'),(9,'Connecticut','636006'),(10,'Delaware','636011'),(11,'District of Columbia','636043'),(12,'Florida','636010'),(13,'Georgia','636055'),(14,'Guam','636019'),(15,'Hawaii','636047'),(16,'Hidalgo','636057'),(17,'Idaho','636050'),(18,'Illinois','636035'),(19,'Indiana','636037'),(20,'Iowa','636018'),(21,'Kansas','636022'),(22,'Kentucky','636046'),(23,'Louisiana','636007'),(24,'Maine','636041'),(25,'Manitoba','636048'),(26,'Maryland','636003'),(27,'Massachusetts','636002'),(28,'Michigan','636032'),(29,'Minnesota','636038'),(30,'Mississippi','636051'),(31,'Missouri','636030'),(32,'Montana','636008'),(33,'Nebraska','636054'),(34,'Nevada','636049'),(35,'New Brunswick','636017'),(36,'New Hampshire','636039'),(37,'New Jersey ','636036'),(38,'New Mexico','636009'),(39,'New York ','636001'),(40,'Newfoundland','636016'),(41,'North Carolina','636004'),(42,'North Dakota','636034'),(43,'Nova Scotia','636013'),(44,'Ohio','636023'),(45,'Oklahoma','636058'),(46,'Ontario','636012'),(47,'Oregon','636029'),(48,'Pennsylvania','636025'),(49,'Prince Edward Island','604425'),(50,'Rhode Island','636052'),(51,'Saskatchewan','636044'),(52,'South Carolina','636005'),(53,'South Dakota','636042'),(54,'State Dept (USA)','636027'),(55,'Tennessee','636053'),(56,'Texas','636015'),(57,'US Virgin Islands','636062'),(58,'Utah','636040'),(59,'Vermont','636024'),(60,'Virginia','636000'),(61,'Washington','636045'),(62,'West Virginia','636061'),(63,'Wisconsin','636031'),(64,'Wyoming','636060');

/*Table structure for table `tb_user` */

DROP TABLE IF EXISTS `tb_user`;

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission` int(2) NOT NULL,
  `rememberme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `club_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `club_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reg_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_user` */

insert  into `tb_user`(`id`,`username`,`email`,`password`,`permission`,`rememberme`,`club_id`,`club_name`,`reg_date`) values (1,'security','security@test.com','$2y$10$WjF5/v34ajmJxH8dEhyA8ua11DNdD3YwL9dumy241DxHBgrWJuBQu',2,'','','','2017-05-05 10:38:14'),(3,'admission','admission@test.com','$2y$10$v6nh5uINX5EHOuVC2c/mT.Uh.q3uKapblODhVchPDYx5ndBjNShIK',3,'','','','2017-05-06 01:45:06'),(5,'security1','sec1@user.com','$2y$10$tmM1Uua6ljRFCIDAJGPejenerR8uafOqjS8an17ToQ.s0M4xiZmIm',2,'','1','John Club','2017-06-19 18:42:38'),(6,'','sec@test.com','$2y$10$8/Dy7H198rsmUHerOG9cTe3CTmcUsv0OFRA7dCaZsSYSeS7NAJ3Qq',2,'','5','Test Club2','2017-07-24 15:58:23');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
